<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organizacion extends Model
{
    protected $fillable = [
        'name', 'details', 'url'
    ];


    public function user()
    {
        $this->hasMany(User::class, 'organizacions_id', 'id');
    }
}
