<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LibrarieTag extends Model
{
    protected $table = 'libraries_tags';

    protected $fillable = ['name', 'details'];

}
