<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsSecction extends Model
{
    protected $fillable = ['name', 'details'];

    protected $table = 'news_secctions';
}
