<?php

namespace App\Http\Controllers;

use App\Country;
use App\Recomendation;
use App\Right;
use App\Typeofrecomendation;
use Illuminate\Http\Request;

class RecomendationController extends Controller
{

    public function index()
    {
        return View('admin.recomendaciones.index');
    }

    public function edit($id)
    {
        $item = Recomendation::findOrFail($id);
        $country = Country::pluck('name', 'id');
        $right = Right::pluck('name', 'id');
        $trec = Typeofrecomendation::pluck('name', 'id');
        return View('admin.recomendaciones.edit', compact('item', 'country', 'right', 'trec'));
    }

    public function update(Request $request, $id)
    {

        $recomendacion = Recomendation::findOrFail($id);

        if($request->has('institution_list')):
            $recomendacion->institutions()->sync($request->input('institution_list'));
        endif;

        if($request->has('derechos_list')):
            $recomendacion->derechos()->sync($request->input('derechos_list'));
        endif;

        $recomendacion->update($request->all());

        return redirect('admin/recomendaciones');

    }

    public function create()
    {
        $country = Country::pluck('name', 'id');
        $right = Right::pluck('name', 'id');
        $trec = Typeofrecomendation::pluck('name', 'id');
        return View('admin.recomendaciones.create', compact('country', 'right', 'trec'));
    }

    public function store(Request $request)
    {
        $recomendacion = Recomendation::create($request->all());

        if($request->has('institution_list')):
            $recomendacion->institutions()->sync($request->input('institution_list'));
        endif;

        if($request->has('derechos_list')):
            $recomendacion->derechos()->sync($request->input('derechos_list'));
        endif;

        return redirect('/admin/recomendaciones');
    }
}
