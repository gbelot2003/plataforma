<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;


class UsersController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return View('admin.users.index');
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        return View('admin.users.edit', compact('user'));
    }

    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        unset($request['id']);
        if($request->input('password')):
            $request['password'] = bcrypt($request->input('password'));
            unset($request['password_confirmation']);
        else:
            unset($request['password']);
            unset($request['password_confirmation']);
        endif;

        $user->update($request->all());

        if($request->input('roles_lists')){
            if(!is_array($request->input('roles_lists'))){
            }else {
                $user->roles()->sync($request->input('roles_lists'));
            }
        }

        return redirect()->to('/admin/users');
    }
    }
