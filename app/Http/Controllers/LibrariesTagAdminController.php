<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LibrariesTagAdminController extends Controller
{

    public function index()
    {
        return View('admin.libraries.tags.index');
    }
}
