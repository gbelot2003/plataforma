<?php

namespace App\Http\Controllers;

use App\Typeofrecomendation;
use Illuminate\Http\Request;

class TrecomendationApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $recom = Typeofrecomendation::orderBy('id', 'DESC')->paginate(15);
        return $recom;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $recom = Typeofrecomendation::create($request->all());
        return 'ok';
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $items = Typeofrecomendation::findOrFail($id);
        return $items;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $items = Typeofrecomendation::findOrFail($id);
        $items->update($request->all());

        return "ok";
    }

    public function search($search = null)
    {
        $query = Typeofrecomendation::where('name', 'LIKE', '%'.$search.'%');
        $query->orWhere('details', 'LIKE', '%'.$search.'%');
        $model = $query->paginate(10);
        return $model;
    }
}
