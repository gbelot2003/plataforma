<?php

namespace App\Http\Controllers;

use App\Organizacion;
use Illuminate\Http\Request;

class OrganizacionApiController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Organizacion::paginate(10);
        return $items;
    }

    /**
     * Search for user registers
     *
     * @param null $search
     * @return mixed
     */
    public function search($search = null){
        $query = Organizacion::where('name', 'LIKE', '%'.$search.'%');
        $query->orWhere('details', 'LIKE', '%'.$search.'%');
        $model = $query->paginate(10);
        return $model;
    }
}
