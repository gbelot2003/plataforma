<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterRequest;
use App\Mail\Verification;
use App\User;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{
    public function create(RegisterRequest $request)
    {

        $user = User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
        ]);

        Mail::to($user->email)->send(new Verification($user));

        return redirect()->to('post-register');
    }


    public function verifiedemal($id, $token)
    {
        $user = User::findOrFail($id);

        if($token === $user->token)
        {
            $user->verified = true;
            $user->save();
            return redirect()->to('/login');
        }
        else {
            return redirect()->to('/'); // TODO: crear pagina que refleje error
        }

    }
}
