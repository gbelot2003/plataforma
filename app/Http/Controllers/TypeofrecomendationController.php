<?php

namespace App\Http\Controllers;

use App\Typeofrecomendation;
use Illuminate\Http\Request;

class TypeofrecomendationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return View('admin.typeofrecomendations.index');
    }
}
