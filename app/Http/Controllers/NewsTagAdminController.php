<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NewsTagAdminController extends Controller
{
    public function index()
    {
        return View('admin.noticias.tags.index');
    }
}
