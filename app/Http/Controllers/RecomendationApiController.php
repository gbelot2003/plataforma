<?php

namespace App\Http\Controllers;

use App\Recomendation;
use Illuminate\Http\Request;

class RecomendationApiController extends Controller
{

    /**
     * @return mixed
     */
    public function index()
    {
        $items = Recomendation::OrderBy('id', 'DESC')->paginate(10);
        return $items;
    }

    /**
     * @param $search
     * @return mixed
     */
    public function search($search)
    {
        $query = Recomendation::where('name', 'LIKE', '%'.$search.'%');
        $model = $query->paginate(10);
        return $model;
    }
}
