<?php

namespace App\Http\Controllers;

use App\Right;
use App\Typeofright;
use Illuminate\Http\Request;

class RightsApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Right::orderBy('id', 'DESC')->paginate(10);
        return $items;
    }


    public function create()
    {
        $rec = Typeofright::all();
        return $rec;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $item = Right::create($request->all());
        return $item;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $rec = Typeofright::all();
        $items = Right::findOrFail($id);
        return $items = ([
            'items' => $items,
            'rec' => $rec
        ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Right::findOrFail($id);
        $item->update($request->all());
        return $item;
    }

    /**
     * Search for user registers
     *
     * @param null $search
     * @return mixed
     */
    public function search($search = null)
    {
        $query = Right::where('name', 'LIKE', '%'.$search.'%');
        $model = $query->paginate(10);
        return $model;
    }
}
