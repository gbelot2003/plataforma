<?php

namespace App\Http\Controllers;

use App\Typeofrecomendation;
use App\Typeofright;
use Illuminate\Http\Request;

class TrightApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Typeofright::paginate(15);
        return $items;
    }


    public function create()
    {
        $rec = Typeofrecomendation::all();
        return $rec;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $item = Typeofright::create($request->all());
        return $item;
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = Typeofright::findOrfail($id);
        $rec = Typeofrecomendation::select('id', 'name')->get();
        return  $item = ([
                'item' => $item,
                'rec' => $rec
            ]);;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Typeofright::findOrfail($id);
        $item->update($request->all());

        return $item;
    }

    /**
     * Search for user registers
     *
     * @param null $search
     * @return mixed
     */
    public function search($search = null){
        $query = Typeofright::where('name', 'LIKE', '%'.$search.'%');
        $model = $query->paginate(10);
        return $model;
    }
}
