<?php

namespace App\Http\Controllers;

use App\LibrarieTag;
use Illuminate\Http\Request;

class LibrariesTagApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $item = LibrarieTag::orderBy('id', 'DESC')->paginate(10);
        return $item;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $item = LibrarieTag::create($request->all());
        return $item;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = LibrarieTag::findOrFail($id);
        return $item;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = LibrarieTag::findOrFail($id);
        $item->update($request->all());
        return $item;
    }

    /**
     * Search for user registers
     *
     * @param null $search
     * @return mixed
     */
    public function search($search = null){
        $query = LibrarieTag::where('name', 'LIKE', '%'.$search.'%');
        $model = $query->paginate(10);
        return $model;
    }
}
