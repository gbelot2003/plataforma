<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;

class UsersApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::paginate(10);
        return $users;
    }

    /**
     * Search for user registers
     *
     * @param null $search
     * @return mixed
     */
    public function search($search = null){
        $query = User::where('name', 'LIKE', '%'.$search.'%');
        $query->orWhere('email', 'LIKE', '%'.$search.'%');
        $model = $query->paginate(10);
        return $model;
    }
}
