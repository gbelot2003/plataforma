<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recomendation extends Model
{

    /**
     * @var array
     * */
    protected $fillable = ['name', 'country_id', 'typeofrecomendations_id', 'details'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function trecomendaciones()
    {
        return $this->belongsTo(Typeofrecomendation::class, 'typeofrecomendations_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function derechos()
    {
        return $this->belongsToMany(Right::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function countries()
    {
        return $this->belongsTo(Country::class, 'country_id', 'id');
    }


    /**
     * @return mixed
     */
/*    public function getInstitutionListAttribute()
    {
        return $this->institutions->pluck('id')->all();
    }*/


    /**
     *
     * @return mixed
     */
    public function getDerechosListAttribute()
    {
        return $this->derechos->pluck('id')->all();
    }
    /**
     * @return mixed
     */
    public function getTrecomendacionListAttribute()
    {
        return $this->trecomendaciones->pluck('id')->all();
    }

}
