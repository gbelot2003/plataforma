<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Institucion extends Model
{
    /**
     * @var string
     */
    public $table = 'institutions';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'name', 'details', 'url'
    ];
}
