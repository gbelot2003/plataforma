<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Right extends Model
{
    public $timestamps = false;


    protected $fillable = [
        'name', 'typeofright_id', 'typeofrecomendation_id'
    ];

    //para simplificar las relaciones las nombramos en español

    /**
     * Relacion a Tipos de derecho
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tipoderecho()
    {
        return $this->belongsTo(Typeofright::class, 'typeofright_id', 'id');
    }

    /**
     * Relación a Tipo de Recomendación
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tiporecomendacion()
    {
        return $this->belongsTo(Typeofrecomendation::class, 'typeofrecomendation_id', 'id');
    }




}
