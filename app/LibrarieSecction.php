<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LibrarieSecction extends Model
{
    protected $table = 'libraries_sections';

    protected $fillable = ['name', 'details'];
}
