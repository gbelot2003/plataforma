<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Typeofrecomendation extends Model
{

    /**
     * @var array
     */
    protected $fillable = [
        'name', 'details'
    ];


    /**
     *
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tiposderechos()
    {
        return $this->hasMany(Typeofright::class);
    }

    /**
     *
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function derechos()
    {
        return $this->hasMany(Right::class);
    }
}


