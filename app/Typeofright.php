<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Typeofright extends Model
{

    public $timestamps = false;


    protected $fillable = [
        'name', 'typeofrecomendation_id'
    ];

    public function tiporecomendacion()
    {
        return $this->belongsTo(Typeofrecomendation::class, 'typeofrecomendation_id', 'id');
    }
}
