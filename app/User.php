<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'users';

    protected $fillable = [
        'name', 'email', 'password', 'organizacions_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public static function boot()
    {
        parent::boot();

        /**
         * generar un string random cada vez que se registre un usaurio
         */
        static::creating(function($user){
            $user->token = str_random(30);
        });

    }


    public function organizacion()
    {
        return $this->belongsTo(Organizacion::class, 'organizacions_id', 'id');
    }
}
