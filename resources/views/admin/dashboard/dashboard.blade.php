@extends('layouts.admin')

@section('siteName', 'Administración')

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ url("/dashboard") }}" class="active">Administración</a></li>
    </ol>
@stop

@section('content')
    @if(Auth::User()->status == false)
        @include('admin.dashboard._non-verified')
    @else
        <ul>
            <li>Mensajes</li>
            <li>Calificaciones Recientes</li>
            <li>Configuraciones de Usuarios</li>
        </ul>
    @endif
@stop