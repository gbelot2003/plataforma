@extends('layouts.admin')

@section('siteName', 'Edicion de Usuarios')

@section('smallTitle')
    Nueva Institución
@stop


@section('content')
    <div class="row">
        <div class="col-md-12">
            {!! Form::open(array('route' => array('instituciones.store'))) !!}
            @include('admin.instituciones._form', array('submitType' => 'Crear'))
            {!! Form::close() !!}
        </div>
    </div>

@stop