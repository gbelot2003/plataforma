@extends('layouts.admin')

@section('siteName', 'Instituciones Gubernamentales')

@section('controller', 'InstitucionesController')

@section('smallTitle', 'Listado de Instituciones Gubernamentales')

@section('linkCreate')
    <a type="button" class="btn btn-info" href="{{ action('InstitucionsAdminController@index') }}">
        <i class="fa fa-plus"></i>
    </a>
@stop

@section('content')

    <div class="row">
        <div class="col-md-12">
            <form>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group has-feedback">
                            <div class="input-group">
                                <span class="input-group-addon">Buscar</span>
                                <input type="text" class="form-control" id="search" ng-model='searchable' aria-describedby="inputGroupSuccess1Status">
                            </div>
                            <span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>
                            <span id="inputGroupSuccess1Status" class="sr-only">(success)</span>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="double-bounce1" ng-show="loader.loading"><span us-spinner></span></div>
            <div class="list-group">
                <a ng-repeat="item in items" ng-click="edit(item.id)" class="list-group-item">@{{ item.name }}</a>
            </div>
        </div>
    </div>
@stop