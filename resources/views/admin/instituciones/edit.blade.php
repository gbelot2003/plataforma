@extends('layouts.admin')

@section('siteName', 'Edicion de Instituciones')

@section('smallTitle')
    Edición de Institución
@stop

@section('smallText')
    {{ $items->name }}
@stop

@section('content')
    <h3>Edición de Organización {{ $items->name }}</h3>

    <div class="row">
        <div class="col-md-12">
            {!! Form::model($items, array('route' => array('instituciones.update', $items->id), 'method' => 'PUT')) !!}
            @include('admin.instituciones._form', array('submitType' => 'Editar'))
            {!! Form::close() !!}
        </div>
    </div>

@stop