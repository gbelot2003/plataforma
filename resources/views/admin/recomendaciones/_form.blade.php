<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label for="name">Recomendación</label>
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">

            <div class="col-md-6">
                <div class="form-group">
                    <label for="name">País</label>
                    {!! Form::select('country_id', $country, null,  ['id' => 'country','class' => 'form-control select2 validate']) !!}
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="name">F.R</label>
                    {!! Form::select('typeofrecomendations_id', $trec, null, ['id' => 'trecomendacion', 'class' => 'califica form-control select2 validate']) !!}
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="name">Tipo</label>
                    {!! Form::select('derechos_list[]', $right, null, ['id' => 'right', 'class' => 'validate form-control select2', 'multiple']) !!}
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="name">Institución</label>
                </div>
            </div>

            <div class="col-md-12">
                <div class="form-group">
                    <label for="details">Detalles</label>
                    {!! Form::textarea('details', null, ['class' => 'ckeditor']) !!}
                </div>
            </div>

        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        {!! Form::submit('Actualizar', ['class' => 'btn btn-info']) !!}
    </div>
</div>