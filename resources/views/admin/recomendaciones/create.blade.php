@extends('layouts.admin')

@section('siteName', 'Crear Recomendación')

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ url("/dashboard") }}" class="">Administración</a></li>
        <li><a href="{{ url("/admin/recomendaciones") }}" class="">Recomendaciones</a></li>
        <li><span  class="active">Crear Recomendación</span></li>
    </ol>
@stop

@section('content')

    {!! Form::open(array('route' => array('recomendaciones.store'))) !!}
        @include('admin.recomendaciones._form')
    {!! Form::close() !!}



    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="//cdn.ckeditor.com/4.6.2/basic/ckeditor.js"></script>

    <script type="text/javascript">
        $('.select2').select2();
    </script>
@stop