@extends('layouts.admin')

@section('siteName', 'Recomendaciones')

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ url("/dashboard") }}" class="">Administración</a></li>
        <li><span  class="active">Recomendaciones</span></li>
    </ol>
@stop

@section('controller', 'RecomendationsController')

@section('content')

    <div class="row">
        <div class="col-md-11">
            <h3>Listado de Recomendaciones</h3>
        </div>

        <div class="col-md-1">
            <a type="button" href="/admin/recomendaciones/create" class="btn btn-info" href="#">
                <i class="fa fa-plus"></i>
            </a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <form>
                <div class="col-md-12">
                    <div class="form-group has-feedback">
                        <div class="input-group">
                            <span class="input-group-addon">Buscar</span>
                            <input type="text" class="form-control" id="search" ng-model='searchable' aria-describedby="inputGroupSuccess1Status">
                        </div>
                        <span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>
                        <span id="inputGroupSuccess1Status" class="sr-only">(success)</span>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="double-bounce1" ng-show="loader.loading"><span us-spinner></span></div>
            <table class="table">
                <thead>
                <tr>
                    <th>No.</th>
                    <th>Recomendacion</th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="item in items">
                    <td>@{{ item.id }}</td>
                    <td><a href="/admin/recomendaciones/@{{ item.id }}/edit">@{{ item.name }}</a></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="row" ng-if="totalItems > 10">
        <div class="col-md-12">
            <ul uib-pagination
                total-items="totalItems"
                num-pages="numPages"
                ng-model="currentPage"
                max-size="maxSize"
                class="pagination-sm"
                boundary-link-numbers="true"
                force-ellipses="true"
                rotate="false"
                ng-click="nextPage(currentPage)">
            </ul>
        </div>
    </div>

@stop
