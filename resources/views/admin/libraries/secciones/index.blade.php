@extends('layouts.admin')

@section('siteName', 'Administración de Secciones de Libreria')

@section('controller', 'LibrariSecctionController')

@section('smallTitle', 'Listado de Secciones de librerias')

@section('linkCreate')
    <a type="button" ng-click="create()" class="btn btn-info" href="#">
        <i class="fa fa-plus"></i>
    </a>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <form>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group has-feedback">
                            <div class="input-group">
                                <span class="input-group-addon">Buscar</span>
                                <input type="text" class="form-control" id="search" ng-model='searchable' aria-describedby="inputGroupSuccess1Status">
                            </div>
                            <span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>
                            <span id="inputGroupSuccess1Status" class="sr-only">(success)</span>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="double-bounce1" ng-show="loader.loading"><span us-spinner></span></div>
            <div class="list-group">
                <a ng-repeat="item in items" ng-click="edit(item.id)" class="list-group-item">@{{ item.name }}</a>
            </div>
        </div>
    </div>

    <div class="row" ng-if="totalItems > 10">
        <div class="col-md-12">
            <ul uib-pagination
                total-items="totalItems"
                num-pages="numPages"
                ng-model="currentPage"
                max-size="maxSize"
                class="pagination-sm"
                boundary-link-numbers="true"
                force-ellipses="true"
                rotate="false"
                ng-click="nextPage(currentPage)">
            </ul>
        </div>
    </div>
@stop