@extends('layouts.admin')

@section('siteName', 'Noticias')


@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ url("/dashboard") }}" class="">Administración</a></li>
        <li><span  class="active">Noticias</span></li>
    </ol>
@stop
