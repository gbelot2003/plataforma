@extends('layouts.admin')

@section('siteName', 'Usuarios')

@section('controller', 'UserController')

@section('smallTitle', 'Listado de usuarios')
@section('smallText', 'Todos los usuarios registrados')

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ url("/dashboard") }}" class="">Administración</a></li>
        <li><a href="#" class="active">Usuarios</a></li>
    </ol>
@stop

@section('content')
    <user-list></user-list>
@stop