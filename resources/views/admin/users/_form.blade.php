<div class="form-group">
    <label for="name">Nombre</label>
    {!! Form::text('name', $user->name, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    <label for="email">E-mail</label>
    {!! Form::text('email', $user->email, ['class' => 'form-control']) !!}
</div>

<div class="row">

    <div class="col-md-6">
        <div class="form-group">
            <label for="password">Contraseña</label>
            {!! Form::password('password', ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label for="password_confirm">Confirmar Contraseña</label>
            {!! Form::password('password_confirm', ['class' => 'form-control']) !!}
        </div>
    </div>

</div>
<hr>
<div class="col-md-12">
    {!! Form::submit($submitType, ['class' => 'btn btn-primary']) !!}
    <hr>
</div>