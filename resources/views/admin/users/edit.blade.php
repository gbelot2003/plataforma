@extends('layouts.admin')

@section('siteName', 'Edicion de Usuarios')

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ url("/dashboard") }}" class="">Administración</a></li>
        <li><a href="/admin/users" class="">Usuarios</a></li>
        <li><span  class="active">{{ $user->name }}</span></li>
    </ol>
@stop

@section('smallTitle')
    Edición de Usuario
@stop

@section('smallText')
    {{ $user->name }}
@stop

@section('content')
    <h3></h3>


    <div class="row">
        <div class="col-md-12">

            {!! Form::model($user, array('route' => array('users.update', $user->id), 'method' => 'patch')) !!}

            @include('admin.users._form', array('submitType' => 'Editar'))
            {!! Form::close() !!}
        </div>
    </div>
@stop
