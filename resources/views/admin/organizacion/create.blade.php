@extends('layouts.admin')

@section('siteName', 'Nueva Organización')

@section('smallTitle', 'Nueva Organización')

@section('content')
    <div class="row">
        <div class="col-md-12">
            {!! Form::open(array('route' => array('organizaciones.store'))) !!}
                @include('admin.organizacion._form', array('submitType' => 'Crear'))
            {!! Form::close() !!}
        </div>
    </div>

@stop