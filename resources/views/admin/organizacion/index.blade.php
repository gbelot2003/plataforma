@extends('layouts.admin')

@section('siteName', 'Organizaciones')

@section('controller', 'OrganizacionController')

@section('smallTitle', 'Listado de Organizaciones')

@section('linkCreate')
    <a type="button" class="btn btn-info" href="{{ action('OrganizacionAdminController@create') }}">
        <i class="fa fa-plus"></i>
    </a>
@stop

@section('content')
   <organiza-list></organiza-list>
@stop