<div class="form-group">
    <label for="name">Nombre</label>
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    <label for="name">Website</label>
    {!! Form::text('url', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    <label for="name">Detalles</label>
    {!! Form::textarea('details', null, ['class' => 'form-control textarea']) !!}
</div>
<hr>
<div class="col-md-12">
    {!! Form::submit($submitType, ['class' => 'btn btn-primary']) !!}
    <hr>
</div>