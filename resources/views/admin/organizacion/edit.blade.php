@extends('layouts.admin')

@section('siteName', 'Edicion de Usuarios')

@section('smallTitle')
    Edición de Organización
@stop

@section('smallText')
    {{ $organizacion->name }}
@stop

@section('content')
    <h3>Edición de Organización {{ $organizacion->name }}</h3>

    <div class="row">
        <div class="col-md-12">
            {!! Form::model($organizacion, array('route' => array('organizaciones.update', $organizacion->id), 'method' => 'PUT')) !!}
                @include('admin.organizacion._form', array('submitType' => 'Editar'))
            {!! Form::close() !!}
        </div>
    </div>

@stop