@extends('layouts.admin')

@section('siteName', 'Secciones Taxonomias')

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ url("/dashboard") }}" class="">Administración</a></li>
        <li><span  class="active">Secciones Taxonomias</span></li>
    </ol>
@stop

@section('content')

    <div class="panel panel-info">
        <!-- Default panel contents -->
        <div class="panel-heading">Secciones y Taxonomia</div>
        <div class="panel-body">
            <p>Listado de terminos taxonomicos usados en la aplicación.</p>
        </div>

        <!-- Table -->
        <table class="table table-striped">
            <tr>
                <th>Nombre</th>
                <th>Detalle</th>
            </tr>
            <tr>
                <td><a href="/admin/taxonomias/tipo-de-recomendaciones">Tipos de recomoendación</a></td>
                <td></td>
            </tr>
            <tr>
                <td><a href="/admin/taxonomias/tipos-de-derechos">Tipos de derechos</a></td>
                <td></td>
            </tr>
            <tr>
                <td><a href="/admin/taxonomias/listado-de-derechos">Listado de derechos</a></td>
                <td></td>
            </tr>

        </table>
    </div>

@stop