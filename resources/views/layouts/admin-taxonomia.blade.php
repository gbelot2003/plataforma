<!DOCTYPE html>
<html lang="en" ng-app="@yield('app', 'app')">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Coalición contra la Impunidad</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
<div>
    @include('layouts.partials._nav')
    <div class="container">
        <div class="row">
            <div class="md-12">
                <h2>@yield('siteName')</h2>
                @yield('breadcrumbs')
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                @include('layouts.partials.taxonomia-well')
            </div>
            <div class="col-md-8 main-content" ng-controller="@yield('controller')">
                @yield('content')
            </div>
        </div>
    </div>
</div>

<!-- Scripts -->
<script src="/js/app.js"></script>
</body>
</html>
