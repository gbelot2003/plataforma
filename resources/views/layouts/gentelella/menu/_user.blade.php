<a href="#" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
    <img src="{{ asset('images/img.jpg') }}" alt="images" />
    {{ Auth::user()->email }} <span class="fa fa-angle-down"></span>
    <ul class="dropdown-menu dropdown-usermenu pull-right">
        <li><a href="javascript:;"> Profile</a></li>
        <li>
            <a href="javascript:;">
                <span class="badge bg-red pull-right">50%</span>
                <span>Settings</span>
            </a>
        </li>
        <li><a href="javascript:;">Help</a></li>
        <li><a href="login.html"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
    </ul>
</a>