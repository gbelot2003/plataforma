<div class="menu_section active">
    <h3>General</h3>
    <ul class="nav side-menu">
        <li><a><i class="fa fa-home"></i> Inicio <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
                <li><a href="{{ action('PagesController@welcome') }}">Portada</a></li>
                <li><a href="{{ action('DashboardController@dashboard') }}">Dashboard</a></li>
            </ul>
        </li>

        <li><a><i class="fa fa-edit"></i> Usuarios <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
                <li><a href="{{ action('UsersController@index') }}">Listado de Usuario</a></li>
            </ul>
        </li>

        <li class="active"><a><i class="fa fa-edit"></i>Organizaciones <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
                <li><a href="{{ action('OrganizacionAdminController@index') }}">Listado de Organizaciones</a></li>
                <li><a href="{{ action('OrganizacionAdminController@create') }}">Crear Organizaciones</a></li>
            </ul>
        </li>

        <li class="active"><a><i class="fa fa-edit"></i> Instituciones <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
                <li><a href="{{ action('InstitucionsAdminController@index') }}">Listado de Instituciones</a></li>{{-- TODO: crear CRUD de Instituciones --}}
                <li><a href="{{ action('InstitucionsAdminController@index') }}">Crear Institución</a></li>
            </ul>
        </li>

        <li class="active"><a><i class="fa fa-edit"></i> Taxonomias <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
                <li><a href="{{ action('TypeofrecomendationController@index') }}">Tipo de recomendación</a></li>
                <li><a href="{{ action('TrightController@index') }}">Tipo de Derechos</a></li>{{-- TODO: CRUD Tipo de derechos--}}
                <li><a href="{{ action('RightsController@index') }}">Listado de derechos</a></li>{{-- TODO: CRUD listado de derechos--}}
                <li><a href="{{ action('LibrariesSecctionAdminController@index') }}">Secciones de Libreria</a></li>{{-- TODO: Inteface de Secciones --}}
                <li><a href="{{ action('LibrariesTagAdminController@index') }}">Tags de Libreria</a></li>{{-- TODO: Inteface de Secciones --}}
                <li><a href="{{ action('NewsSecctionAdminController@index') }}">Secciones de Notícias</a></li>{{-- TODO: Inteface de Secciones --}}
                <li><a href="{{ action('NewsTagAdminController@index') }}">Tags de Notícias</a></li>{{-- TODO: Inteface de Secciones --}}
            </ul>
        </li>

        <li class="active"><a><i class="fa fa-edit"></i> Recomendaciones <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
                <li><a href="{{ action('RecomendationController@index') }}">Listado de Recomendaciones</a></li>
                <li><a href="{{ action('RecomendationController@create') }}">Crear Institución</a></li>
            </ul>
        </li>

        <li class="active"><a><i class="fa fa-edit"></i> Calificaciones <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
                <li><a href="{{ action('RecomendationController@index') }}">Listado de Recomendaciones</a></li>{{--TODO: Interface de Calificaciones--}}
                <li><a href="{{ action('RecomendationController@create') }}">Crear Institución</a></li>
            </ul>
        </li>

        <li class="active"><a><i class="fa fa-edit"></i> Biblioteca <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
                <li><a href="{{ action('RecomendationController@index') }}">Listado de Documentos</a></li>{{--TODO: Interface de Documentos--}}
                <li><a href="{{ action('RecomendationController@index') }}">Listado de Videos</a></li>{{--TODO: Interface de Videos--}}
                <li><a href="{{ action('RecomendationController@index') }}">Listado de Audío</a></li>{{--TODO: Interface de Audio--}}
            </ul>
        </li>

        <li class="active"><a><i class="fa fa-edit"></i> Noticias <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
                <li><a href="{{ action('NewsAdminController@index') }}">Listado de Noticias</a></li>{{--TODO: Interface de Calificaciones--}}
                <li><a href="{{ action('NewsAdminController@index') }}">Crear Noticias</a></li>
            </ul>
        </li>
    </ul>
</div>