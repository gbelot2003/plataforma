<div class="well">
    @if(!Auth::User()->verified == false)
        <div class="list-group">

            <a href="/admin/libreria-virtual" class="list-group-item {{ Request::is('admin/libreria-virtual') ? 'active' : '' }}">
                <h4 class="list-group-item-heading">Biblioteca Virtual</h4>
                <p class="list-group-item-text">Administración de Biblioteca virtual y secciones</p>
            </a>

            <a href="/admin/libreria-virtual/libreria" class="list-group-item {{ Request::is('admin/libreria-virtual/libreria') ? 'active' : '' }}">
                <h4 class="list-group-item-heading">Administración de Libros Virtuales</h4>
                <p class="list-group-item-text">Administración de Libreria virtual</p>
            </a>

            <a href="/admin/libreria-virtual/secciones" class="list-group-item {{ Request::is('admin/libreria-virtual/secciones') ? 'active' : '' }}">
                <h4 class="list-group-item-heading">Administración de Secciones</h4>
                <p class="list-group-item-text">Administración de Secciones en libreria virtual</p>
            </a>

            <a href="/admin/libreria-virtual/tags" class="list-group-item {{ Request::is('admin/libreria-virtual/tags') ? 'active' : '' }}">
                <h4 class="list-group-item-heading">Administración de Tags</h4>
                <p class="list-group-item-text">Administración de Tags en libreria virtual</p>
            </a>


        </div>
    @else
        <div class="list-group">
            <a href="#" class="list-group-item">
                <h4 class="list-group-item-heading">Configuración de Perfil</h4>
                <p class="list-group-item-text">Información personal y la organización a la que perteneces</p>
            </a>
        </div>
    @endif
</div>