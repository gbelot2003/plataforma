<div class="well">
    @if(!Auth::User()->verified == false)
    <div class="list-group">

        <a href="/admin/taxonomias" class="list-group-item {{ Request::is('admin/taxonomias') ? 'active' : '' }}">
            <h4 class="list-group-item-heading">Secciones Taxonómicas</h4>
            <p class="list-group-item-text">Sección de listados taxonomicos, secciones y paises</p>
        </a>

        <a href="/admin/taxonomias/tipo-de-recomendaciones" class="list-group-item {{ Request::is('admin/taxonomias/tipo-de-recomendaciones') ? 'active' : '' }}">
            <h4 class="list-group-item-heading">Tipos de Recomendaciones</h4>
            <p class="list-group-item-text">Listados de Tipos de recomendaciones</p>
        </a>

        <a href="/admin/taxonomias/tipos-de-derechos" class="list-group-item {{ Request::is('admin/taxonomias/tipos-de-derechos') ? 'active' : '' }}">
            <h4 class="list-group-item-heading">Tipos de Derechos</h4>
            <p class="list-group-item-text">Listados de Tipos de derechos</p>
        </a>

        <a href="/admin/taxonomias/listado-de-derechos" class="list-group-item {{ Request::is('admin/taxonomias/listado-de-derechos') ? 'active' : '' }}">
            <h4 class="list-group-item-heading">Listado de Derechos</h4>
            <p class="list-group-item-text">Listados de derechos</p>
        </a>


    </div>
    @else
    <div class="list-group">
        <a href="#" class="list-group-item">
            <h4 class="list-group-item-heading">Configuración de Perfil</h4>
            <p class="list-group-item-text">Información personal y la organización a la que perteneces</p>
        </a>
    </div>
    @endif
</div>