<div class="well">
    @if(!Auth::User()->verified == false)
        <div class="list-group">

            <a href="/dashboard" class="list-group-item {{ Request::is('dashboard') ? 'active' : '' }} ">
                <h4 class="list-group-item-heading">Administración</h4>
                <p class="list-group-item-text">Sección de administración personal</p>
            </a>


            <a href="/admin/users" class="list-group-item {{ Request::is('admin/users') ? 'active' : '' }}">
                <h4 class="list-group-item-heading">Usuarios</h4>
                <p class="list-group-item-text">Administración de usuarios, permisos y estados</p>
            </a>


            <a href="/admin/organizaciones" class="list-group-item {{ Request::is('admin/organizaciones') ? 'active' : '' }}">
                <h4 class="list-group-item-heading">Organizaciones</h4>
                <p class="list-group-item-text">Listado general de organizaciones aliadas</p>
            </a>


            <a href="/admin/instituciones" class="list-group-item {{ Request::is('admin/instituciones') ? 'active' : '' }}">
                <h4 class="list-group-item-heading">Instituciones de gobierno</h4>
                <p class="list-group-item-text">Listado instituciones calificadas</p>
            </a>


            <a href="/admin/taxonomias" class="list-group-item {{ Request::is('admin/taxonomias') ? 'active' : '' }}">
                <h4 class="list-group-item-heading">Secciones Taxonómicas</h4>
                <p class="list-group-item-text">Sección de listados taxonomicos, secciones y paises</p>
            </a>


            <a href="/admin/recomendaciones" class="list-group-item {{ Request::is('admin/recomendaciones') ? 'active' : '' }}">
                <h4 class="list-group-item-heading">Recomendaciones</h4>
                <p class="list-group-item-text">Listado general de recomendaciones</p>
            </a>


            <a href="#" class="list-group-item">
                <h4 class="list-group-item-heading">Calificaciones</h4>
                <p class="list-group-item-text">Administración y revisión de Calificaciones</p>
            </a>


            <a href="/admin/libreria-virtual" class="list-group-item {{ Request::is('admin/libreria-virtual') ? 'active' : '' }}">
                <h4 class="list-group-item-heading">Biblioteca virtual</h4>
                <p class="list-group-item-text">Administración de Biblioteca</p>
            </a>


            <a href="/admin/noticias" class="list-group-item {{ Request::is('admin/noticias') ? 'active' : '' }}">
                <h4 class="list-group-item-heading">Noticias</h4>
                <p class="list-group-item-text">Sección de administración personal</p>
            </a>


            <a href="#" class="list-group-item">
                <h4 class="list-group-item-heading">Calendario y eventos</h4>
                <p class="list-group-item-text">Sección de administración personal</p>
            </a>


            <a href="#" class="list-group-item">
                <h4 class="list-group-item-heading">Configuración de Perfil</h4>
                <p class="list-group-item-text">Información personal y la organización a la que perteneces</p>
            </a>


        </div>
    @else
        <div class="list-group">
            <a href="#" class="list-group-item">
                <h4 class="list-group-item-heading">Configuración de Perfil</h4>
                <p class="list-group-item-text">Información personal y la organización a la que perteneces</p>
            </a>
        </div>
     @endif
</div>