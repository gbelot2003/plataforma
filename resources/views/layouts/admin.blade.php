<!DOCTYPE html>
<html lang="en" ng-app="@yield('app', 'app')">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Coalición contra la Impunidad</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body class="nav-md">

<div class="container body" ng-controller = @yield('controller')>
    <div class="main_container">
        <toast></toast>
        <div class="col-md-3 left_col">
            <div class="left-col scroll-view">
                @include('layouts.gentelella.leftbar._profile')
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    @include('layouts.gentelella.leftbar._menu')
                </div>
            </div>
        </div>

        <div class="top_nav">
            @include('layouts.gentelella.topnav._nav_menu')
        </div>
        <div class="right_col" role="main" style="min-height: 768px">
            <div>
                <div class="page-title">
                    <div class="title_left">
                        <h3>@yield('siteName')</h3>
                    </div>
                    {{--<div class="title_right">
                        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search for...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">Go!</button>
                                </span>
                            </div>
                        </div>
                    </div>--}}
                </div>

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>@yield('smallTitle')
                                <small>@yield('smallText')</small>
                                </h2>
                                @include('layouts.gentelella.topnav.panel_toolbox')
                                <div class="clearfix"></div>
                            </div>

                            <div class="x_content">
                                <div class="col-md-8 col-lg-8 col-sm-7">

                                    @yield('content')
                                </div>

                                <div class="col-md-4 col-lg-4 col-sm-5">{{--TODO: Limpiar y configurar Dashboar --}}
                                    <h1>h1. Bootstrap heading</h1>
                                    <h2>h2. Bootstrap heading</h2>
                                    <h3>h3. Bootstrap heading</h3>
                                    <h4>h4. Bootstrap heading</h4>
                                    <h5>h5. Bootstrap heading</h5>
                                    <h6>h6. Bootstrap heading</h6>
                                </div>

                                <div class="clearfix"></div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/admin.js') }}"></script>
</body>
</html>
