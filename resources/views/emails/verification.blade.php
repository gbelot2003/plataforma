<h2>Te has suscrito a nuestra plataforma</h2>

Para continuar debes seguir el siguiente  <a href="{{ url('/verify-token/' .$user->id, $user->token) }}">link</a>

<p>
    Luego de ingresar, por favor llena el formulario que a continuación se presentara con la información solicitada para
    acredirte como usuario y que puedas tener acceso a las acciones que necesitas.
</p>