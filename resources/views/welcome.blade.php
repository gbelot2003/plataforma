@extends('layouts.app')

@section('siteName', 'Welcome Page')

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ url("/") }}" class="active">Inicio</a></li>
    </ol>
@stop

@section('content')
    <p>Esta es una muestra del poder de Laravel</p>
@stop
