
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */



require('./bootstrap');
const app = angular.module('app',
    [
        'ngResource',
        'ngSanitize',
        'ngFileUpload',
        'ngAnimate',
        'ngTouch',
        'ngToast',
        'angularSpinner',
        'ui.bootstrap',
        'ui.select'
    ]
);

require('./components/')(app);
