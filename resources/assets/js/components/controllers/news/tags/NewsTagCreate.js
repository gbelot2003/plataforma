const rights = function($scope, NewsTagFactory, $uibModalInstance){

    $scope.loader = {
        loading: false
    };

    $scope.loader.loading = true;

    $scope.init = function(){
        $scope.items = {};
        $scope.loader.loading = false;
    };


    $scope.ok = function () {

        NewsTagFactory.save($scope.items).$promise.then(
            function success(response){
                $uibModalInstance.close(true);
            }
        );
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };

    $scope.init();
};

module.exports = function(app){
    app.controller('NewsTagCreate', function($scope, NewsTagFactory, $uibModalInstance){
        return rights($scope, NewsTagFactory, $uibModalInstance);
    });
};