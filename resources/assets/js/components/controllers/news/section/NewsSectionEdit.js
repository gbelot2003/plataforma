const items = function($scope, NewsSectionFactory, $uibModalInstance, id){

    $scope.loader = {
        loading: false
    };

    $scope.loader.loading = true;

    $scope.init = function(){
        NewsSectionFactory.get({id : id}, function success(response){
            $scope.items = response;
            $scope.options = response.rec;
            $scope.totalItems = response.total;
            $scope.currentPage = response.current_page;
            $scope.maxSize = response.per_page;
            $scope.loader.loading = false;
        });
    };

    $scope.ok = function () {

        NewsSectionFactory.update($scope.items).$promise.then(
            function success(response){
                $uibModalInstance.close(true);
            }
        );
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };

    $scope.init();
};

module.exports = function(app){
    app.controller('NewsSectionEdit', function($scope, NewsSectionFactory, $uibModalInstance, id){
        return items($scope, NewsSectionFactory, $uibModalInstance, id);
    });
};