const rights = function($scope, NewsSectionFactory, $uibModalInstance){

    $scope.loader = {
        loading: false
    };

    $scope.loader.loading = true;

    $scope.init = function(){
        $scope.items = {};
        $scope.loader.loading = false;
    };


    $scope.ok = function () {

        NewsSectionFactory.save($scope.items).$promise.then(
            function success(response){
                $uibModalInstance.close(true);
            }
        );
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };

    $scope.init();
};

module.exports = function(app){
    app.controller('NewsSectionCreate', function($scope, NewsSectionFactory, $uibModalInstance){
        return rights($scope, NewsSectionFactory, $uibModalInstance);
    });
};