module.exports = function(app){
    require('./section/NewsSectionController.js')(app);
    require('./section/NewsSectionEdit.js')(app);
    require('./section/NewsSectionCreate.js')(app);
    require('./tags/NewsTagController.js')(app);
    require('./tags/NewsTagEdit.js')(app);
    require('./tags/NewsTagCreate.js')(app);
};