const rights = function($scope, InstitucionesFactory, $uibModalInstance, ngToast){

    $scope.loader = {
        loading: false
    };

    $scope.loader.loading = true;

    $scope.init = function(){
        $scope.items = {};
        $scope.loader.loading = false;
    };


    $scope.ok = function () {

        InstitucionesFactory.save($scope.items).$promise.then(
            function success(response){
                $uibModalInstance.close(true);
            }
        );
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };

    $scope.init();
};

module.exports = function(app){
    app.controller('InstitucionesCreateController', function($scope, InstitucionesFactory, $uibModalInstance, ngToast){
        return rights($scope, InstitucionesFactory, $uibModalInstance, ngToast);
    });
};