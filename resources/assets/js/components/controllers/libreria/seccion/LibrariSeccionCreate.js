const rights = function($scope, LibreriaSeccionFactory, $uibModalInstance, ngToast){

    $scope.loader = {
        loading: false
    };

    $scope.loader.loading = true;

    $scope.init = function(){
        $scope.items = {};
        $scope.loader.loading = false;
    };


    $scope.ok = function () {

        LibreriaSeccionFactory.save($scope.items).$promise.then(
            function success(response){
                $uibModalInstance.close(true);
            }
        );
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };

    $scope.init();
};

module.exports = function(app){
    app.controller('LibrariSeccionCreate', function($scope, LibreriaSeccionFactory, $uibModalInstance, ngToast){
        return rights($scope, LibreriaSeccionFactory, $uibModalInstance, ngToast);
    });
};