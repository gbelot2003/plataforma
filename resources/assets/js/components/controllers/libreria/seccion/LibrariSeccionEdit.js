const items = function($scope, LibreriaSeccionFactory, $uibModalInstance, id, ngToast){

    $scope.loader = {
        loading: false
    };

    $scope.loader.loading = true;

    $scope.init = function(){
        LibreriaSeccionFactory.get({id : id}, function success(response){
            $scope.items = response;
            $scope.options = response.rec;
            $scope.totalItems = response.total;
            $scope.currentPage = response.current_page;
            $scope.maxSize = response.per_page;
            $scope.loader.loading = false;
        });
    };

    $scope.ok = function () {

        LibreriaSeccionFactory.update($scope.items).$promise.then(
            function success(response){
                $uibModalInstance.close(true);
            }
        );
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };

    $scope.init();
};

module.exports = function(app){
    app.controller('LibrariSeccionEdit', function($scope, LibreriaSeccionFactory, $uibModalInstance, id, ngToast){
        return items($scope, LibreriaSeccionFactory, $uibModalInstance, id, ngToast);
    });
};