module.exports = function(app){
    require('./seccion/LibrariSecctionController.js')(app);
    require('./seccion/LibrariSeccionEdit.js')(app);
    require('./seccion/LibrariSeccionCreate.js')(app);
    require('./tags/LibrariTagsController.js')(app);
    require('./tags/LibrariTagsEditController.js')(app);
    require('./tags/LibrariTagsCreateController.js')(app);
};