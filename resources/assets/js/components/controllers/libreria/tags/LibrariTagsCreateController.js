const rights = function($scope, LibreriaTagsFactory, $uibModalInstance, ngToast){

    $scope.loader = {
        loading: false
    };

    $scope.loader.loading = true;

    $scope.init = function(){
        $scope.items = {};
        $scope.loader.loading = false;
    };


    $scope.ok = function () {

        LibreriaTagsFactory.save($scope.items).$promise.then(
            function success(response){
                $uibModalInstance.close(true);
            }
        );
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };

    $scope.init();
};

module.exports = function(app){
    app.controller('LibrariTagsCreateController', function($scope, LibreriaTagsFactory, $uibModalInstance, ngToast){
        return rights($scope, LibreriaTagsFactory, $uibModalInstance, ngToast);
    });
};