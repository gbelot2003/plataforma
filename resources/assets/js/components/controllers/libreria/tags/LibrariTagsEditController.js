const items = function($scope, LibreriaTagsFactory, $uibModalInstance, id, ngToast){

    $scope.loader = {
        loading: false
    };

    $scope.loader.loading = true;

    $scope.init = function(){
        LibreriaTagsFactory.get({id : id}, function success(response){
            $scope.items = response;
            $scope.options = response.rec;
            $scope.totalItems = response.total;
            $scope.currentPage = response.current_page;
            $scope.maxSize = response.per_page;
            $scope.loader.loading = false;
        });
    };

    $scope.ok = function () {

        LibreriaTagsFactory.update($scope.items).$promise.then(
            function success(response){
                $uibModalInstance.close(true);
            }
        );
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };

    $scope.init();
};

module.exports = function(app){
    app.controller('LibrariTagsEditController', function($scope, LibreriaTagsFactory, $uibModalInstance, id, ngToast){
        return items($scope, LibreriaTagsFactory, $uibModalInstance, id, ngToast);
    });
};