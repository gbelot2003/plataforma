const item = function($scope, LibreriaTagsFactory,  ngToast, $uibModal){

    $scope.loader = {
        loading: false
    };

    $scope.init = function() {
        $scope.loader.loading = true;

        LibreriaTagsFactory.get().$promise
            .then(function success(response){
                $scope.items = response.data;
                $scope.totalItems = response.total;
                $scope.currentPage = response.current_page;
                $scope.maxSize = response.per_page;
                $scope.loader.loading = false;
            }, function error(response){
                ngToast.danger('A habido algun error, el servidor responde ' + response.sendText);
            });

    };

    $scope.search = function(val){
        $scope.searcher = true;
        $scope.loader.loading2 = true;

        LibreriaTagsFactory.query({query:val}, function(response){
            $scope.items = response.data;
            $scope.totalItems = response.total;
            $scope.currentPage = response.current_page;
            $scope.maxSize = response.per_page;
            $scope.loader.loading2 = false;
        }, function error(response){
            ngToast.danger('A habido algun error, el servidor responde ' + response.sendText);
            $scope.loader.loading = false;
        });

    };

    $scope.$watch('searchable', function(newVal, oldVal){
        if(oldVal === newVal) return;
        if(newVal === ""){
            $scope.init();
            $scope.searcher = false;
        } else {
            $scope.newVal = newVal;
            $scope.search(newVal);
        }
    });


    $scope.setPage = function (pageNo) {
        $scope.currentPage = pageNo;
    };

    $scope.nextPage = function(pageNo) {
        $scope.loader.loading = true;
        $scope.currentPage = pageNo;


        var query = LibreriaTagsFactory.get({page:$scope.currentPage});

        if($scope.searcher == true){
            var query = LibreriaTagsFactory.query({query:$scope.newVal, page:$scope.currentPage});
        } else {
            query.$promise.then(function success(response){
                $scope.items = response.data;
                $scope.totalItems = response.total;
                $scope.currentPage = response.current_page;
                $scope.maxSize = response.per_page;
                $scope.loader.loading = false;
            }, function error(response){
                ngToast.danger('A habido algun error, el servidor responde ' + response.sendText);
                $scope.loader.loading = false;

            });
        }

    };

    $scope.edit = function(id){

        var modalInstance = $uibModal.open({
            animation: true,
            template: require('raw!./ltag-edit.html'),
            controller: 'LibrariTagsEditController',
            backdrop: 'static',
            resolve: {
                id: id
            }
        });

        modalInstance.result.then(function(message){
            if(message == true){
                ngToast.success('El Tag se a actualizado correctamente!!');
                $scope.init();
            } else {
                ngToast.danger('A ocurrido un error en la actualización del permiso');
            }
        });

    };

    $scope.create = function(){
        var modalInstance = $uibModal.open({
            animation: true,
            template: require('raw!./ltag-create.html'),
            controller: 'LibrariTagsCreateController',
            backdrop: 'static'
        });

        modalInstance.result.then(function(message){
            if(message == true){
                ngToast.success('El Tag se a creado correctamente!!');
                $scope.init();
            } else {
                ngToast.danger('A ocurrido un error en la actualización del permiso');
            }
        });
    };


    $scope.init();
};

module.exports = function(app){
    app.controller('LibrariTagsController', function($scope, LibreriaTagsFactory,  ngToast, $uibModal){
        return item($scope, LibreriaTagsFactory,  ngToast, $uibModal);
    });
};