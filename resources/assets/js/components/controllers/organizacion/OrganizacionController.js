const organizacion = function($scope, OrganizacionFactory){

    $scope.loader = {
        loading: false
    };

    $scope.init = function() {
        $scope.loader.loading = true;

        OrganizacionFactory.get().$promise
            .then(function success(response){
                $scope.organizacions = response.data;
                $scope.totalItems = response.total;
                $scope.currentPage = response.current_page;
                $scope.maxSize = response.per_page;
                $scope.loader.loading = false;
            }, function error(response){
                ngToast.danger('A habido algun error, el servidor responde ' + response.sendText);
            });

    };

    $scope.search = function(val){
        $scope.searcher = true;
        $scope.loader.loading2 = true;

        OrganizacionFactory.query({query:val}, function(response){
            $scope.organizacions = response.data;
            $scope.totalItems = response.total;
            $scope.currentPage = response.current_page;
            $scope.maxSize = response.per_page;
            $scope.loader.loading2 = false;
        }, function error(response){
            ngToast.danger('A habido algun error, el servidor responde ' + response.sendText);
            $scope.loader.loading = false;
        });

    };

    $scope.$watch('searchable', function(newVal, oldVal){
        if(oldVal === newVal) return;
        if(newVal === ""){
            $scope.init();
            $scope.searcher = false;
        } else {
            $scope.newVal = newVal;
            $scope.search(newVal);
        }
    });


    $scope.setPage = function (pageNo) {
        $scope.currentPage = pageNo;
    };

    $scope.nextPage = function(pageNo) {
        $scope.loader.loading = true;
        $scope.currentPage = pageNo;


        var query = OrganizacionFactory.get({page:$scope.currentPage});

        if($scope.searcher == true){
            var query = OrganizacionFactory.query({query:$scope.newVal, page:$scope.currentPage});
        } else {
            query.$promise.then(function success(response){
                $scope.organizacions = response.data;
                $scope.totalItems = response.total;
                $scope.currentPage = response.current_page;
                $scope.maxSize = response.per_page;
                $scope.loader.loading = false;
            }, function error(response){
                ngToast.danger('A habido algun error, el servidor responde ' + response.sendText);
                $scope.loader.loading = false;

            });
        }

    };

    $scope.init();
};

module.exports = function(app){
    app.controller('OrganizacionController', function($scope, OrganizacionFactory){
        return organizacion($scope, OrganizacionFactory);
    });
};