const user = function($scope, UserFactory, ngToast){

    $scope.loader = {
        loading: false
    };

    $scope.init = function(){
        $scope.loader.loading = true;

        UserFactory.get(function (response){
            $scope.users = response.data;
            $scope.totalItems = response.total;
            $scope.currentPage = response.current_page;
            $scope.maxSize = response.per_page;
            //console.log($scope.totalItems);// TODO: remover scope
            $scope.loader.loading = false;
        }, function error(){
            ngToast.danger('A habido algun error, el servidor responde ' + response.sendText);
        });
    };


    $scope.search = function(val){
        $scope.searcher = true;
        $scope.loader.loading2 = true;

        UserFactory.query({query:val}, function(response){
            $scope.users = response.data;
            $scope.totalItems = response.total;
            $scope.currentPage = response.current_page;
            $scope.maxSize = response.per_page;
            $scope.loader.loading2 = false;
        }, function error(response){
            ngToast.danger('A habido algun error, el servidor responde ' + response.sendText);
            $scope.loader.loading = false;
        });

    };

    $scope.$watch('searchable', function(newVal, oldVal){
        if(oldVal === newVal) return;
        if(newVal === ""){
            $scope.init();
            $scope.searcher = false;
        } else {
            $scope.newVal = newVal;
            $scope.search(newVal);
        }
    });


    $scope.setPage = function (pageNo) {
        $scope.currentPage = pageNo;
    };

    $scope.nextPage = function(pageNo) {
        $scope.loader.loading = true;
        $scope.currentPage = pageNo;


        var query = UserFactory.get({page:$scope.currentPage});

        if($scope.searcher == true){
            var query = UserFactory.query({query:$scope.newVal, page:$scope.currentPage});
        } else {
            query.$promise.then(function success(response){
                $scope.users = response.data;
                $scope.totalItems = response.total;
                $scope.currentPage = response.current_page;
                $scope.maxSize = response.per_page;
                $scope.loader.loading = false;
            }, function error(response){
                ngToast.danger('A habido algun error, el servidor responde ' + response.sendText);
                $scope.loader.loading = false;

            });
        }

    };


    $scope.init();
};

module.exports = function(app){
    app.controller('UserController', function($scope, UserFactory, ngToast){
        return user($scope, UserFactory, ngToast);
    });
};