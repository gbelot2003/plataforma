const trights = function($scope, TypeofrightsFactoy, $uibModalInstance, id, ngToast){

    $scope.loader = {
        loading: false
    };

    $scope.loader.loading = true;

    $scope.init = function(){
        TypeofrightsFactoy.get({id : id}, function success(response){
            $scope.items = response.item;
            $scope.options = response.rec;
            $scope.totalItems = response.total;
            $scope.currentPage = response.current_page;
            $scope.maxSize = response.per_page;
            $scope.loader.loading = false;
        });
    };

    $scope.ok = function () {

        TypeofrightsFactoy.update($scope.items).$promise.then(
            function success(response){
                $uibModalInstance.close(true);
            }
        );
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };

    $scope.init();
};

module.exports = function(app){
    app.controller('TypeofrightsEditController', function($scope, TypeofrightsFactoy, $uibModalInstance, id, ngToast){
        return trights($scope, TypeofrightsFactoy, $uibModalInstance, id, ngToast);
    });
};