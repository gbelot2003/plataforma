const trights = function($scope, TypeofrightsFactoy, ngToast, $uibModal){

    $scope.loader = {
        loading: false
    };

    $scope.init = function() {
        $scope.loader.loading = true;

        TypeofrightsFactoy.get().$promise
            .then(function success(response){
                $scope.items = response.data;
                $scope.totalItems = response.total;
                $scope.currentPage = response.current_page;
                $scope.maxSize = response.per_page;
                $scope.loader.loading = false;
            }, function error(response){
                ngToast.danger('A habido algun error, el servidor responde ' + response.sendText);
            });
    };

    $scope.search = function(val){
        $scope.searcher = true;
        $scope.loader.loading2 = true;

        TypeofrightsFactoy.query({query:val}, function(response){
            $scope.items = response.data;
            $scope.totalItems = response.total;
            $scope.currentPage = response.current_page;
            $scope.maxSize = response.per_page;
            $scope.loader.loading2 = false;
        }, function error(response){
            ngToast.danger('A habido algun error, el servidor responde ' + response.sendText);
            $scope.loader.loading = false;
        });

    };

    $scope.$watch('searchable', function(newVal, oldVal){
        if(oldVal === newVal) return;
        if(newVal === ""){
            $scope.init();
            $scope.searcher = false;
        } else {
            $scope.newVal = newVal;
            $scope.search(newVal);
        }
    });


    $scope.setPage = function (pageNo) {
        $scope.currentPage = pageNo;
    };

    $scope.nextPage = function(pageNo) {
        $scope.loader.loading = true;
        $scope.currentPage = pageNo;


        var query = TypeofrightsFactoy.get({page:$scope.currentPage});

        if($scope.searcher == true){
            var query = TypeofrightsFactoy.query({query:$scope.newVal, page:$scope.currentPage});
        } else {
            query.$promise.then(function success(response){
                $scope.items = response.data;
                $scope.totalItems = response.total;
                $scope.currentPage = response.current_page;
                $scope.maxSize = response.per_page;
                $scope.loader.loading = false;
            }, function error(response){
                ngToast.danger('A habido algun error, el servidor responde ' + response.sendText);
                $scope.loader.loading = false;

            });
        }

    };

    $scope.edit = function(id){

        var modalInstance = $uibModal.open({
            animation: true,
            template: require('raw!./trights-edit.html'),
            controller: 'TypeofrightsEditController',
            backdrop: 'static',
            resolve: {
                id: id
            }
        });

        modalInstance.result.then(function(message){
            if(message == true){
                ngToast.success('El tipo de derecho se a actualizado correctamente!!');
                $scope.init();
            } else {
                ngToast.danger('A ocurrido un error en la actualización del permiso');
            }
        });

    };

    $scope.create = function(){
        var modalInstance = $uibModal.open({
            animation: true,
            template: require('raw!./trights-create.html'),
            controller: 'TypeofrightsCreateController',
            backdrop: 'static'
        });

        modalInstance.result.then(function(message){
            if(message == true){
                ngToast.success('El tipo de derecho se a actualizado correctamente!!');
                $scope.init();
            } else {
                ngToast.danger('A ocurrido un error en la actualización del permiso');
            }
        });
    };


    $scope.init();


};

module.exports = function(app){
    app.controller('TypeofrightsController', function($scope, TypeofrightsFactoy, ngToast, $uibModal){
        return trights($scope, TypeofrightsFactoy, ngToast, $uibModal);
    });
};