const trights = function($scope, TypeofrightsFactoy, $uibModalInstance, ngToast){

    $scope.loader = {
        loading: false
    };

    $scope.loader.loading = true;

    $scope.init = function(){

        TypeofrightsFactoy.create(function success(response){
            $scope.options = response;
            $scope.loader.loading = false;
        });
        $scope.items = {};
    };


    $scope.ok = function () {

        TypeofrightsFactoy.save($scope.items).$promise.then(
            function success(response){
                $scope.options = response.rec;
                $uibModalInstance.close(true);
            }
        );
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };

    $scope.init();
};

module.exports = function(app){
    app.controller('TypeofrightsCreateController', function($scope, TypeofrightsFactoy, $uibModalInstance, ngToast){
        return trights($scope, TypeofrightsFactoy, $uibModalInstance, ngToast);
    });
};