const rec = function($scope, recomendationFactory, ngToast, $uibModal){

    $scope.loader = {
        loading: false
    };

    $scope.init = function() {
        $scope.loader.loading = true;
        recomendationFactory.get().$promise
        .then(function success(response){
            $scope.items = response.data;
            $scope.totalItems = response.total;
            $scope.currentPage = response.current_page;
            $scope.maxSize = response.per_page;
            $scope.loader.loading = false;

            console.log($scope.currentPage);
        });
    };

    $scope.search = function(val){
        $scope.searcher = true;
        $scope.loader.loading2 = true;

        recomendationFactory.query({query:val}, function(response){
            $scope.items = response.data;
            $scope.totalItems = response.total;
            $scope.currentPage = response.current_page;
            $scope.maxSize = response.per_page;
            $scope.loader.loading2 = false;
        }, function error(response){
            ngToast.danger('A habido algun error, el servidor responde ' + response.sendText);
            $scope.loader.loading = false;
        });

    };

    $scope.$watch('searchable', function(newVal, oldVal){
        if(oldVal === newVal) return;
        if(newVal === ""){
            $scope.init();
            $scope.searcher = false;
        } else {
            $scope.newVal = newVal;
            $scope.search(newVal);
        }
    });

    $scope.setPage = function (pageNo) {
        $scope.currentPage = pageNo;
    };

    $scope.nextPage = function(pageNo) {
        $scope.loader.loading = true;
        $scope.currentPage = pageNo;


        var query = recomendationFactory.get({page:$scope.currentPage});

        if($scope.searcher == true){
            var query = recomendationFactory.query({query:$scope.newVal, page:$scope.currentPage});
        } else {
            query.$promise.then(function success(response){
                $scope.items = response.data;
                $scope.totalItems = response.total;
                $scope.currentPage = response.current_page;
                $scope.maxSize = response.per_page;
                $scope.loader.loading = false;
            }, function error(response){
                ngToast.danger('A habido algun error, el servidor responde ' + response.sendText);
                $scope.loader.loading = false;
            });
        }

    };

    $scope.init();

};

module.exports = function(app){
    app.controller('RecomendationsController', function($scope, recomendationFactory, ngToast, $uibModal){
        return rec($scope, recomendationFactory, ngToast, $uibModal);
    });
};