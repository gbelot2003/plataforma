const TrecomendacionCreate = function($scope, TrecomendationsFactory, $uibModalInstance, ngToast){

    $scope.loader = {
        loading: false
    };

    $scope.loader.loading = true;

    $scope.init = function(){
            $scope.items = {};
            $scope.loader.loading = false;
    };


    $scope.ok = function () {

        TrecomendationsFactory.save($scope.items).$promise.then(
            function success(response){
                $uibModalInstance.close(true);
            }
        );
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };

    $scope.init();
};

module.exports = function(app){
    app.controller('TrecomendationCreateController', function($scope, TrecomendationsFactory, $uibModalInstance, ngToast){
        return TrecomendacionCreate($scope, TrecomendationsFactory, $uibModalInstance, ngToast);
    });
};