const TrecomendacionEdit = function($scope, TrecomendationsFactory, $uibModalInstance, id, ngToast){

    $scope.loader = {
        loading: false
    };

    $scope.loader.loading = true;

    $scope.init = function(){
        TrecomendationsFactory.get({id : id}, function success(response){
            $scope.items = response;
            $scope.totalItems = response.total;
            $scope.currentPage = response.current_page;
            $scope.maxSize = response.per_page;
            $scope.loader.loading = false;
        });
    };


    $scope.ok = function () {

        TrecomendationsFactory.update($scope.items).$promise.then(
            function success(response){
                $uibModalInstance.close(true);
            }
        );
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };

    $scope.init();
};

module.exports = function(app){
    app.controller('TrecomendationEditController', function($scope, TrecomendationsFactory, $uibModalInstance, id, ngToast){
        return TrecomendacionEdit($scope, TrecomendationsFactory, $uibModalInstance, id, ngToast);
    });
};