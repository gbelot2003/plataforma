const rights = function($scope, rightsFactory, $uibModalInstance, ngToast){

    $scope.loader = {
        loading: false
    };

    $scope.loader.loading = true;

    $scope.items = {};
    $scope.init = function(){
        rightsFactory.create(function success(response){
            $scope.options = response;
            console.log($scope.options)
            $scope.loader.loading = false;
        });

    };


    $scope.ok = function () {

        rightsFactory.save($scope.items).$promise.then(
            function success(response){
                $uibModalInstance.close(true);
            }
        );
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };

    $scope.init();
};

module.exports = function(app){
    app.controller('rightsCreateController', function($scope, rightsFactory, $uibModalInstance, ngToast){
        return rights($scope, rightsFactory, $uibModalInstance, ngToast);
    });
};