module.exports = function(app){
    app.directive('userList', function(){
        return {
            controller: 'UserController',
            template: require("raw!./user-list.html")
        }
    });
};