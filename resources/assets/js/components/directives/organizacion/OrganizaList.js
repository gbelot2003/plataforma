module.exports = function(app){
    app.directive('organizaList', function(){
        return {
            controller: 'OrganizacionController',
            template: require("raw!./organiza-list.html")
        }
    });
};