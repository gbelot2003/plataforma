module.exports = function(app){
    app.factory('UserFactory', function($resource) {
        return $resource('/api/users/:id', { id: '@id', page:'@page'}, {
            'update':{method:'PUT'},
            'query': {method:'GET',
                params:{ query:"@query", page:"@page"},
                url:'/api/users/search/:query'
            },
        });
    });
};