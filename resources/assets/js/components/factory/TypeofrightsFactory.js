module.exports = function(app){
    app.factory('TypeofrightsFactoy', function($resource) {
        return $resource('/api/taxonomias/tipos-de-derechos/:id', { id: '@id', page:'@page'}, {
            'update':{method:'PUT'},
            'create':{method: 'GET',
                isArray:true,
                url:'/api/taxonomias/tipos-de-derechos/create'
            },
            'query': {method:'GET',
                params:{ query:"@query", page:"@page"},
                url:'/api/taxonomias/tipos-de-derechos/search/:query'
            },
        });
    });
};