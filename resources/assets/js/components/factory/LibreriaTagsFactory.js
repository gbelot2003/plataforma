module.exports = function(app){
    app.factory('LibreriaTagsFactory', function($resource) {
        return $resource('/api/taxonomias/tags-de-libreria/:id', { id: '@id', page:'@page'}, {
            'update':{method:'PUT'},
            'query': {method:'GET',
                params:{ query:"@query", page:"@page"},
                url:'/api/taxonomias/tags-de-libreria/search/:query'
            },
        });
    });
};