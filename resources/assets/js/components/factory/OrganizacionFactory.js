module.exports = function(app){
    app.factory('OrganizacionFactory', function($resource) {
        return $resource('/api/organizaciones/:id', { id: '@id', page:'@page'}, {
            'update':{method:'PUT'},
            'query': {method:'GET',
                params:{ query:"@query", page:"@page"},
                url:'/api/organizaciones/search/:query'
            },
        });
    });
};