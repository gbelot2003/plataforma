module.exports = function(app){
    require('./UserFactory')(app);
    require('./OrganizacionFactory')(app);
    require('./TrecomendationsFactory')(app);
    require('./TypeofrightsFactory')(app);
    require('./rightsFactory')(app);
    require('./rightsFactory')(app);
    require('./recomendationFactory')(app);
    require('./InstitucionesFactory')(app);
    require('./LibreriaTagsFactory')(app);
    require('./LibreriaSeccionFactory')(app);
    require('./NewsSectionFactory')(app);
    require('./NewsTagFactory')(app);
};