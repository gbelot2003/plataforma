module.exports = function(app){
    app.factory('NewsSectionFactory', function($resource) {
        return $resource('/api/taxonomias/secciones-de-noticias/:id', { id: '@id', page:'@page'}, {
            'update':{method:'PUT'},
            'query': {method:'GET',
                params:{ query:"@query", page:"@page"},
                url:'/api/taxonomias/secciones-de-noticias/search/:query'
            },
        });
    });
};