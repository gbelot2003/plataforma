module.exports = function(app){
    app.factory('rightsFactory', function($resource) {
        return $resource('/api/taxonomias/listado-de-derechos/:id', { id: '@id', page:'@page'}, {
            'update':{method:'PUT'},
            'create':{method: 'GET',
                isArray:true,
                url:'/api/taxonomias/listado-de-derechos/create'
            },
            'query': {method:'GET',
                params:{ query:"@query", page:"@page"},
                url:'/api/taxonomias/listado-de-derechos/search/:query'
            },
        });
    });
};