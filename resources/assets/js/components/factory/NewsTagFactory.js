module.exports = function(app){
    app.factory('NewsTagFactory', function($resource) {
        return $resource('/api/taxonomias/tags-de-Noticias/:id', { id: '@id', page:'@page'}, {
            'update':{method:'PUT'},
            'query': {method:'GET',
                params:{ query:"@query", page:"@page"},
                url:'/api/taxonomias/tags-de-Noticias/search/:query'
            },
        });
    });
};