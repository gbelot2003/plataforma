module.exports = function(app){
    app.factory('InstitucionesFactory', function($resource) {
        return $resource('/api/instituciones/:id', { id: '@id', page:'@page'}, {
            'update':{method:'PUT'},
            'query': {method:'GET',
                params:{ query:"@query", page:"@page"},
                url:'/api/instituciones/search/:query'
            },
        });
    });
};