module.exports = function(app){
    app.factory('LibreriaSeccionFactory', function($resource) {
        return $resource('/api/taxonomias/secciones-de-libreria/:id', { id: '@id', page:'@page'}, {
            'update':{method:'PUT'},
            'query': {method:'GET',
                params:{ query:"@query", page:"@page"},
                url:'/api/taxonomias/secciones-de-libreria/search/:query'
            },
        });
    });
};