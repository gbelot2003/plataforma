module.exports = function(app){
    app.factory('TrecomendationsFactory', function($resource) {
        return $resource('/api/taxonomias/tipo-de-recomendaciones/:id', { id: '@id', page:'@page'}, {
            'update':{method:'PUT'},
            'query': {method:'GET',
                params:{ query:"@query", page:"@page"},
                url:'/api/taxonomias/tipo-de-recomendaciones/search/:query'
            },
        });
    });
};