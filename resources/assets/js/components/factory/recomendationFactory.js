module.exports = function(app){
    app.factory('recomendationFactory', function($resource) {
        return $resource('/api/recomendaciones/:id', { id: '@id', page:'@page'}, {
            'update':{method:'PUT'},
            'query': {method:'GET',
                params:{ query:"@query", page:"@page"},
                url:'/api/recomendaciones/search/:query'
            },
        });
    });
};