module.exports = function(app){
    require('./factory')(app);
    require('./directives')(app);
    require('./controllers')(app);
};