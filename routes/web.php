<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'PagesController@welcome');

Auth::routes();
// Overwrite la ruta de register
Route::post('/register', 'RegisterController@create');
Route::get('/verify-token/{id}/{token}', 'RegisterController@verifiedemal'); //TODO: agregar middleware que garantize que solo alguien verificado puede ingresar
Route::get('/post-register', 'PagesController@postregister');


Route::group(['middleware' => 'auth'], function () {

    Route::get('dashboard', 'DashboardController@dashboard');

    Route::group(['prefix' => 'admin'], function () {

        Route::resource("users", 'UsersController', ['only' => ['index', 'edit', 'update']]);
        Route::resource('organizaciones', 'OrganizacionAdminController', ['except' => ['show', 'destroy']]);
        Route::get('instituciones', 'InstitucionsAdminController@index');
        Route::get('taxonomias', 'TaxonomyController@index');
        Route::get('taxonomias/tipo-de-recomendaciones', 'TypeofrecomendationController@index');
        Route::get('taxonomias/tipos-de-derechos', 'TrightController@index');
        Route::get('taxonomias/listado-de-derechos', 'RightsController@index');
        Route::get('taxonomias/tags-de-libreria', 'LibrariesTagAdminController@index');
        Route::get('taxonomias/secciones-de-libreria', 'LibrariesSecctionAdminController@index');
        Route::get('taxonomias/secciones-de-Noticias', 'NewsSecctionAdminController@index');
        Route::get('taxonomias/tags-de-Noticias', 'NewsTagAdminController@index');

        Route::resource('recomendaciones', 'RecomendationController', ['except' => ['show', 'destroy']]);

        Route::get('noticias', 'NewsAdminController@index');



        // TODO: relacionar bases de datos mediante modelos

    });

    Route::group(['prefix' => 'api'], function () {

        Route::resource('users', 'UsersApiController', ['only' => ['index']]);
        Route::get('users/search/{search?}', 'UsersApiController@search');

        Route::get('organizaciones', 'OrganizacionApiController@index');
        Route::get('organizaciones/search/{search?}', 'OrganizacionApiController@search');

        Route::resource('instituciones', 'InstitucionesApiController', ['except' => ['edit', 'destroy', 'create']]);
        Route::get('instituciones/search/{search?}', 'InstitucionesApiController@search');

        Route::resource('recomendaciones', 'RecomendationApiController', ['only' => ['index']]);
        Route::get('recomendaciones/search/{search?}', 'RecomendationApiController@search');

        Route::resource('taxonomias/tipo-de-recomendaciones', 'TrecomendationApiController', ['except' => ['edit', 'destroy', 'create']]);
        Route::get('taxonomias/tipo-de-recomendaciones/search/{search?}', 'TrecomendationApiController@search');

        Route::resource('taxonomias/tipos-de-derechos', 'TrightApiController', ['except' => ['edit', 'destroy']]);
        Route::get('taxonomias/tipos-de-derechos/search/{search?}', 'TrightApiController@search');

        Route::resource('taxonomias/listado-de-derechos', 'RightsApiController', ['except' => ['edit', 'destroy']]);
        Route::get('taxonomias/listado-de-derechos/search/{search?}', 'RightsApiController@search');

        Route::resource('taxonomias/tags-de-libreria', 'LibrariesTagApiController', ['except' => ['edit', 'destroy', 'create']]);
        Route::get('taxonomias/tags-de-libreria/search/{search?}', 'LibrariesTagApiController@search');

        Route::resource('taxonomias/secciones-de-libreria', 'LibrariesSecctionApiController', ['except' => ['edit', 'destroy', 'create']]);
        Route::get('taxonomias/secciones-de-libreria/search/{search?}', 'LibrariesTagApiController@search');

        Route::resource('taxonomias/secciones-de-noticias', 'NewsSecctionApiController', ['except' => ['edit', 'destroy', 'create']]);
        Route::get('taxonomias/secciones-de-noticias/search/{search?}', 'NewsSecctionApiController@search');

        Route::resource('taxonomias/tags-de-Noticias', 'NewsTagApiController', ['except' => ['edit', 'destroy', 'create']]);
        Route::get('taxonomias/taxonomias/tags-de-Noticias/search/{search?}', 'NewsTagApiController@search');

    });

});
