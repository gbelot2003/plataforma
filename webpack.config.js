var webpack = require('webpack');
module.exports = {
    module: {
        loader:[
            {
                test:/\.html$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'raw'
            },
        ]
    }
};