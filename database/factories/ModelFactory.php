<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'verified' => true,
        'status' => true,
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\LibrarieTag::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'details' => $faker->sentence
    ];
});

$factory->define(App\LibrarieSecction::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'details' => $faker->sentence
    ];
});


$factory->define(App\NewsSecction::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->sentence(),
    ];
});


$factory->define(App\NewsTag::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->sentence(),
    ];
});