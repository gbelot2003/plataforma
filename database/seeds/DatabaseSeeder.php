<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(OrganizacionsTableSeeder::class);
        $this->call(InstitutionsTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(TypeofrightsTableSeeder::class);
        $this->call(TypeofrecomendationsTableSeeder::class);
        $this->call(RightsTableSeeder::class);
        $this->call(RecomendationsTableSeeder::class);
        $this->call(RecomendationRightTableSeeder::class);
        $this->call(InstitutionRecomendationTableSeeder::class);
        $this->call(LibrariesTagsTableSeeder::class);
        $this->call(LibrariesSectionsTableSeeder::class);
        $this->call(NewsTagsTableSeeder::class);
        $this->call(NewsSecctionsTableSeeder::class);
    }
}
