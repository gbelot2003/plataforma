<?php

use Illuminate\Database\Seeder;

class LibrariesSectionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('libraries_sections')->delete();
        
        \DB::table('libraries_sections')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Rosanna Deckow',
                'details' => 'Mollitia nisi laborum et et repudiandae sapiente.',
                'created_at' => '2017-02-14 12:45:42',
                'updated_at' => '2017-02-14 12:45:42',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Keshaun Rolfson',
                'details' => 'Minus laudantium dolore vel voluptas nulla consectetur aliquam.',
                'created_at' => '2017-02-14 12:45:42',
                'updated_at' => '2017-02-14 12:45:42',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Juston McCullough',
                'details' => 'Beatae fugiat necessitatibus illum ut architecto deleniti sequi nam.',
                'created_at' => '2017-02-14 12:45:42',
                'updated_at' => '2017-02-14 12:45:42',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Braxton Bogisich',
                'details' => 'Blanditiis odit incidunt incidunt iure.',
                'created_at' => '2017-02-14 12:45:42',
                'updated_at' => '2017-02-14 12:45:42',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Annamarie Bernhard',
                'details' => 'Est dolore vel quo omnis qui hic accusamus.',
                'created_at' => '2017-02-14 12:45:42',
                'updated_at' => '2017-02-14 12:45:42',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Alverta Wolff PhD',
                'details' => 'Et qui natus ab est id consequatur.',
                'created_at' => '2017-02-14 12:45:42',
                'updated_at' => '2017-02-14 12:45:42',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Greg Hudson',
                'details' => 'Minima dolor dolores ipsam qui praesentium voluptas.',
                'created_at' => '2017-02-14 12:45:42',
                'updated_at' => '2017-02-14 12:45:42',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Mr. Kaden Brakus DVM',
                'details' => 'Sunt et voluptas dolor officiis minima aut facere aliquam.',
                'created_at' => '2017-02-14 12:45:42',
                'updated_at' => '2017-02-14 12:45:42',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Yoshiko Thiel',
                'details' => 'Quod omnis eum quibusdam ipsa rerum eveniet doloremque illo.',
                'created_at' => '2017-02-14 12:45:42',
                'updated_at' => '2017-02-14 12:45:42',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Courtney Kuhlman',
                'details' => 'Sunt aspernatur nesciunt amet enim.',
                'created_at' => '2017-02-14 12:45:42',
                'updated_at' => '2017-02-14 12:45:42',
            ),
        ));
        
        
    }
}