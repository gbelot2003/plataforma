<?php

use Illuminate\Database\Seeder;

class RecomendationRightTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('recomendation_right')->delete();
        
        \DB::table('recomendation_right')->insert(array (
            0 => 
            array (
                'right_id' => 9,
                'recomendation_id' => 1,
            ),
            1 => 
            array (
                'right_id' => 9,
                'recomendation_id' => 2,
            ),
            2 => 
            array (
                'right_id' => 8,
                'recomendation_id' => 2,
            ),
            3 => 
            array (
                'right_id' => 12,
                'recomendation_id' => 8,
            ),
            4 => 
            array (
                'right_id' => 19,
                'recomendation_id' => 9,
            ),
            5 => 
            array (
                'right_id' => 8,
                'recomendation_id' => 10,
            ),
            6 => 
            array (
                'right_id' => 12,
                'recomendation_id' => 10,
            ),
            7 => 
            array (
                'right_id' => 3,
                'recomendation_id' => 11,
            ),
            8 => 
            array (
                'right_id' => 8,
                'recomendation_id' => 12,
            ),
            9 => 
            array (
                'right_id' => 19,
                'recomendation_id' => 13,
            ),
            10 => 
            array (
                'right_id' => 7,
                'recomendation_id' => 14,
            ),
            11 => 
            array (
                'right_id' => 11,
                'recomendation_id' => 15,
            ),
            12 => 
            array (
                'right_id' => 5,
                'recomendation_id' => 16,
            ),
            13 => 
            array (
                'right_id' => 11,
                'recomendation_id' => 17,
            ),
            14 => 
            array (
                'right_id' => 12,
                'recomendation_id' => 17,
            ),
            15 => 
            array (
                'right_id' => 9,
                'recomendation_id' => 18,
            ),
            16 => 
            array (
                'right_id' => 7,
                'recomendation_id' => 19,
            ),
            17 => 
            array (
                'right_id' => 12,
                'recomendation_id' => 20,
            ),
            18 => 
            array (
                'right_id' => 5,
                'recomendation_id' => 21,
            ),
            19 => 
            array (
                'right_id' => 12,
                'recomendation_id' => 21,
            ),
            20 => 
            array (
                'right_id' => 13,
                'recomendation_id' => 22,
            ),
            21 => 
            array (
                'right_id' => 7,
                'recomendation_id' => 23,
            ),
            22 => 
            array (
                'right_id' => 11,
                'recomendation_id' => 24,
            ),
            23 => 
            array (
                'right_id' => 7,
                'recomendation_id' => 25,
            ),
            24 => 
            array (
                'right_id' => 8,
                'recomendation_id' => 26,
            ),
            25 => 
            array (
                'right_id' => 5,
                'recomendation_id' => 27,
            ),
            26 => 
            array (
                'right_id' => 19,
                'recomendation_id' => 28,
            ),
            27 => 
            array (
                'right_id' => 12,
                'recomendation_id' => 29,
            ),
            28 => 
            array (
                'right_id' => 5,
                'recomendation_id' => 29,
            ),
            29 => 
            array (
                'right_id' => 12,
                'recomendation_id' => 30,
            ),
            30 => 
            array (
                'right_id' => 7,
                'recomendation_id' => 31,
            ),
            31 => 
            array (
                'right_id' => 12,
                'recomendation_id' => 32,
            ),
            32 => 
            array (
                'right_id' => 4,
                'recomendation_id' => 33,
            ),
            33 => 
            array (
                'right_id' => 11,
                'recomendation_id' => 34,
            ),
            34 => 
            array (
                'right_id' => 6,
                'recomendation_id' => 35,
            ),
            35 => 
            array (
                'right_id' => 7,
                'recomendation_id' => 36,
            ),
            36 => 
            array (
                'right_id' => 2,
                'recomendation_id' => 37,
            ),
            37 => 
            array (
                'right_id' => 12,
                'recomendation_id' => 38,
            ),
            38 => 
            array (
                'right_id' => 22,
                'recomendation_id' => 39,
            ),
            39 => 
            array (
                'right_id' => 24,
                'recomendation_id' => 40,
            ),
            40 => 
            array (
                'right_id' => 25,
                'recomendation_id' => 41,
            ),
            41 => 
            array (
                'right_id' => 8,
                'recomendation_id' => 41,
            ),
            42 => 
            array (
                'right_id' => 9,
                'recomendation_id' => 42,
            ),
            43 => 
            array (
                'right_id' => 5,
                'recomendation_id' => 43,
            ),
            44 => 
            array (
                'right_id' => 3,
                'recomendation_id' => 44,
            ),
            45 => 
            array (
                'right_id' => 12,
                'recomendation_id' => 44,
            ),
            46 => 
            array (
                'right_id' => 6,
                'recomendation_id' => 45,
            ),
            47 => 
            array (
                'right_id' => 26,
                'recomendation_id' => 46,
            ),
            48 => 
            array (
                'right_id' => 11,
                'recomendation_id' => 47,
            ),
            49 => 
            array (
                'right_id' => 12,
                'recomendation_id' => 48,
            ),
            50 => 
            array (
                'right_id' => 10,
                'recomendation_id' => 49,
            ),
            51 => 
            array (
                'right_id' => 12,
                'recomendation_id' => 50,
            ),
            52 => 
            array (
                'right_id' => 12,
                'recomendation_id' => 51,
            ),
            53 => 
            array (
                'right_id' => 12,
                'recomendation_id' => 52,
            ),
            54 => 
            array (
                'right_id' => 4,
                'recomendation_id' => 53,
            ),
            55 => 
            array (
                'right_id' => 7,
                'recomendation_id' => 54,
            ),
            56 => 
            array (
                'right_id' => 12,
                'recomendation_id' => 55,
            ),
            57 => 
            array (
                'right_id' => 10,
                'recomendation_id' => 56,
            ),
            58 => 
            array (
                'right_id' => 2,
                'recomendation_id' => 57,
            ),
            59 => 
            array (
                'right_id' => 5,
                'recomendation_id' => 58,
            ),
            60 => 
            array (
                'right_id' => 7,
                'recomendation_id' => 59,
            ),
            61 => 
            array (
                'right_id' => 14,
                'recomendation_id' => 60,
            ),
            62 => 
            array (
                'right_id' => 5,
                'recomendation_id' => 61,
            ),
            63 => 
            array (
                'right_id' => 6,
                'recomendation_id' => 62,
            ),
            64 => 
            array (
                'right_id' => 1,
                'recomendation_id' => 63,
            ),
            65 => 
            array (
                'right_id' => 8,
                'recomendation_id' => 64,
            ),
            66 => 
            array (
                'right_id' => 25,
                'recomendation_id' => 64,
            ),
            67 => 
            array (
                'right_id' => 2,
                'recomendation_id' => 65,
            ),
            68 => 
            array (
                'right_id' => 2,
                'recomendation_id' => 66,
            ),
            69 => 
            array (
                'right_id' => 27,
                'recomendation_id' => 67,
            ),
            70 => 
            array (
                'right_id' => 25,
                'recomendation_id' => 68,
            ),
            71 => 
            array (
                'right_id' => 12,
                'recomendation_id' => 69,
            ),
            72 => 
            array (
                'right_id' => 12,
                'recomendation_id' => 70,
            ),
            73 => 
            array (
                'right_id' => 12,
                'recomendation_id' => 71,
            ),
            74 => 
            array (
                'right_id' => 15,
                'recomendation_id' => 72,
            ),
            75 => 
            array (
                'right_id' => 10,
                'recomendation_id' => 73,
            ),
            76 => 
            array (
                'right_id' => 24,
                'recomendation_id' => 74,
            ),
            77 => 
            array (
                'right_id' => 8,
                'recomendation_id' => 75,
            ),
            78 => 
            array (
                'right_id' => 15,
                'recomendation_id' => 76,
            ),
            79 => 
            array (
                'right_id' => 24,
                'recomendation_id' => 77,
            ),
            80 => 
            array (
                'right_id' => 5,
                'recomendation_id' => 78,
            ),
            81 => 
            array (
                'right_id' => 12,
                'recomendation_id' => 78,
            ),
            82 => 
            array (
                'right_id' => 7,
                'recomendation_id' => 79,
            ),
            83 => 
            array (
                'right_id' => 7,
                'recomendation_id' => 80,
            ),
            84 => 
            array (
                'right_id' => 12,
                'recomendation_id' => 81,
            ),
            85 => 
            array (
                'right_id' => 27,
                'recomendation_id' => 82,
            ),
            86 => 
            array (
                'right_id' => 5,
                'recomendation_id' => 83,
            ),
            87 => 
            array (
                'right_id' => 19,
                'recomendation_id' => 84,
            ),
            88 => 
            array (
                'right_id' => 19,
                'recomendation_id' => 85,
            ),
            89 => 
            array (
                'right_id' => 27,
                'recomendation_id' => 86,
            ),
            90 => 
            array (
                'right_id' => 5,
                'recomendation_id' => 87,
            ),
            91 => 
            array (
                'right_id' => 22,
                'recomendation_id' => 88,
            ),
            92 => 
            array (
                'right_id' => 11,
                'recomendation_id' => 89,
            ),
            93 => 
            array (
                'right_id' => 8,
                'recomendation_id' => 90,
            ),
            94 => 
            array (
                'right_id' => 26,
                'recomendation_id' => 91,
            ),
            95 => 
            array (
                'right_id' => 12,
                'recomendation_id' => 92,
            ),
            96 => 
            array (
                'right_id' => 3,
                'recomendation_id' => 93,
            ),
            97 => 
            array (
                'right_id' => 22,
                'recomendation_id' => 94,
            ),
            98 => 
            array (
                'right_id' => 8,
                'recomendation_id' => 95,
            ),
            99 => 
            array (
                'right_id' => 10,
                'recomendation_id' => 96,
            ),
            100 => 
            array (
                'right_id' => 19,
                'recomendation_id' => 96,
            ),
            101 => 
            array (
                'right_id' => 5,
                'recomendation_id' => 97,
            ),
            102 => 
            array (
                'right_id' => 2,
                'recomendation_id' => 98,
            ),
            103 => 
            array (
                'right_id' => 6,
                'recomendation_id' => 99,
            ),
            104 => 
            array (
                'right_id' => 5,
                'recomendation_id' => 100,
            ),
            105 => 
            array (
                'right_id' => 24,
                'recomendation_id' => 101,
            ),
            106 => 
            array (
                'right_id' => 7,
                'recomendation_id' => 102,
            ),
            107 => 
            array (
                'right_id' => 11,
                'recomendation_id' => 102,
            ),
            108 => 
            array (
                'right_id' => 14,
                'recomendation_id' => 103,
            ),
            109 => 
            array (
                'right_id' => 3,
                'recomendation_id' => 104,
            ),
            110 => 
            array (
                'right_id' => 5,
                'recomendation_id' => 105,
            ),
            111 => 
            array (
                'right_id' => 4,
                'recomendation_id' => 106,
            ),
            112 => 
            array (
                'right_id' => 12,
                'recomendation_id' => 106,
            ),
            113 => 
            array (
                'right_id' => 5,
                'recomendation_id' => 107,
            ),
            114 => 
            array (
                'right_id' => 10,
                'recomendation_id' => 108,
            ),
            115 => 
            array (
                'right_id' => 7,
                'recomendation_id' => 109,
            ),
            116 => 
            array (
                'right_id' => 20,
                'recomendation_id' => 110,
            ),
            117 => 
            array (
                'right_id' => 19,
                'recomendation_id' => 111,
            ),
            118 => 
            array (
                'right_id' => 11,
                'recomendation_id' => 112,
            ),
            119 => 
            array (
                'right_id' => 1,
                'recomendation_id' => 113,
            ),
            120 => 
            array (
                'right_id' => 8,
                'recomendation_id' => 114,
            ),
            121 => 
            array (
                'right_id' => 7,
                'recomendation_id' => 115,
            ),
            122 => 
            array (
                'right_id' => 25,
                'recomendation_id' => 116,
            ),
            123 => 
            array (
                'right_id' => 12,
                'recomendation_id' => 117,
            ),
            124 => 
            array (
                'right_id' => 7,
                'recomendation_id' => 118,
            ),
            125 => 
            array (
                'right_id' => 10,
                'recomendation_id' => 119,
            ),
            126 => 
            array (
                'right_id' => 22,
                'recomendation_id' => 120,
            ),
            127 => 
            array (
                'right_id' => 12,
                'recomendation_id' => 121,
            ),
            128 => 
            array (
                'right_id' => 7,
                'recomendation_id' => 122,
            ),
            129 => 
            array (
                'right_id' => 8,
                'recomendation_id' => 123,
            ),
            130 => 
            array (
                'right_id' => 2,
                'recomendation_id' => 124,
            ),
            131 => 
            array (
                'right_id' => 12,
                'recomendation_id' => 125,
            ),
            132 => 
            array (
                'right_id' => 5,
                'recomendation_id' => 126,
            ),
            133 => 
            array (
                'right_id' => 12,
                'recomendation_id' => 127,
            ),
            134 => 
            array (
                'right_id' => 2,
                'recomendation_id' => 128,
            ),
            135 => 
            array (
                'right_id' => 22,
                'recomendation_id' => 129,
            ),
            136 => 
            array (
                'right_id' => 8,
                'recomendation_id' => 130,
            ),
            137 => 
            array (
                'right_id' => 12,
                'recomendation_id' => 130,
            ),
            138 => 
            array (
                'right_id' => 24,
                'recomendation_id' => 131,
            ),
            139 => 
            array (
                'right_id' => 5,
                'recomendation_id' => 132,
            ),
            140 => 
            array (
                'right_id' => 2,
                'recomendation_id' => 133,
            ),
            141 => 
            array (
                'right_id' => 5,
                'recomendation_id' => 133,
            ),
            142 => 
            array (
                'right_id' => 1,
                'recomendation_id' => 134,
            ),
            143 => 
            array (
                'right_id' => 9,
                'recomendation_id' => 135,
            ),
            144 => 
            array (
                'right_id' => 7,
                'recomendation_id' => 136,
            ),
            145 => 
            array (
                'right_id' => 7,
                'recomendation_id' => 137,
            ),
            146 => 
            array (
                'right_id' => 20,
                'recomendation_id' => 138,
            ),
            147 => 
            array (
                'right_id' => 23,
                'recomendation_id' => 139,
            ),
            148 => 
            array (
                'right_id' => 14,
                'recomendation_id' => 139,
            ),
            149 => 
            array (
                'right_id' => 6,
                'recomendation_id' => 140,
            ),
            150 => 
            array (
                'right_id' => 5,
                'recomendation_id' => 141,
            ),
            151 => 
            array (
                'right_id' => 5,
                'recomendation_id' => 142,
            ),
            152 => 
            array (
                'right_id' => 14,
                'recomendation_id' => 142,
            ),
            153 => 
            array (
                'right_id' => 18,
                'recomendation_id' => 141,
            ),
            154 => 
            array (
                'right_id' => 18,
                'recomendation_id' => 143,
            ),
            155 => 
            array (
                'right_id' => 26,
                'recomendation_id' => 144,
            ),
            156 => 
            array (
                'right_id' => 6,
                'recomendation_id' => 145,
            ),
            157 => 
            array (
                'right_id' => 12,
                'recomendation_id' => 145,
            ),
            158 => 
            array (
                'right_id' => 8,
                'recomendation_id' => 146,
            ),
            159 => 
            array (
                'right_id' => 6,
                'recomendation_id' => 147,
            ),
            160 => 
            array (
                'right_id' => 25,
                'recomendation_id' => 148,
            ),
            161 => 
            array (
                'right_id' => 5,
                'recomendation_id' => 149,
            ),
            162 => 
            array (
                'right_id' => 9,
                'recomendation_id' => 149,
            ),
            163 => 
            array (
                'right_id' => 18,
                'recomendation_id' => 149,
            ),
            164 => 
            array (
                'right_id' => 24,
                'recomendation_id' => 150,
            ),
            165 => 
            array (
                'right_id' => 8,
                'recomendation_id' => 151,
            ),
            166 => 
            array (
                'right_id' => 12,
                'recomendation_id' => 151,
            ),
            167 => 
            array (
                'right_id' => 2,
                'recomendation_id' => 152,
            ),
            168 => 
            array (
                'right_id' => 10,
                'recomendation_id' => 153,
            ),
            169 => 
            array (
                'right_id' => 12,
                'recomendation_id' => 154,
            ),
            170 => 
            array (
                'right_id' => 3,
                'recomendation_id' => 155,
            ),
            171 => 
            array (
                'right_id' => 7,
                'recomendation_id' => 155,
            ),
            172 => 
            array (
                'right_id' => 12,
                'recomendation_id' => 156,
            ),
            173 => 
            array (
                'right_id' => 18,
                'recomendation_id' => 157,
            ),
            174 => 
            array (
                'right_id' => 2,
                'recomendation_id' => 158,
            ),
            175 => 
            array (
                'right_id' => 12,
                'recomendation_id' => 159,
            ),
            176 => 
            array (
                'right_id' => 5,
                'recomendation_id' => 160,
            ),
            177 => 
            array (
                'right_id' => 14,
                'recomendation_id' => 160,
            ),
            178 => 
            array (
                'right_id' => 6,
                'recomendation_id' => 161,
            ),
            179 => 
            array (
                'right_id' => 7,
                'recomendation_id' => 162,
            ),
            180 => 
            array (
                'right_id' => 12,
                'recomendation_id' => 163,
            ),
            181 => 
            array (
                'right_id' => 14,
                'recomendation_id' => 164,
            ),
        ));
        
        
    }
}
