<?php

use Illuminate\Database\Seeder;

class LibrariesTagsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('libraries_tags')->delete();
        
        \DB::table('libraries_tags')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Gerhard McLaughlin',
                'details' => 'Ut mollitia quia vel dolores.',
                'created_at' => '2017-02-14 12:44:30',
                'updated_at' => '2017-02-14 12:44:30',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Dr. Ignatius Dooley',
                'details' => 'Repellendus reiciendis praesentium quia totam.',
                'created_at' => '2017-02-14 12:44:30',
                'updated_at' => '2017-02-14 12:44:30',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Ole Runte',
                'details' => 'Nobis distinctio autem impedit totam vero libero soluta.',
                'created_at' => '2017-02-14 12:44:30',
                'updated_at' => '2017-02-14 12:44:30',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Josefina Adams',
                'details' => 'Omnis sit voluptas fugit voluptatem dicta.',
                'created_at' => '2017-02-14 12:44:30',
                'updated_at' => '2017-02-14 12:44:30',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Kaitlin Steuber',
                'details' => 'Delectus et laudantium unde voluptas.',
                'created_at' => '2017-02-14 12:44:30',
                'updated_at' => '2017-02-14 12:44:30',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Audra Gleason Jr.',
                'details' => 'Distinctio fugiat modi et sint.',
                'created_at' => '2017-02-14 12:44:30',
                'updated_at' => '2017-02-14 12:44:30',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Muriel Feil',
                'details' => 'Nihil quia eaque quae provident.',
                'created_at' => '2017-02-14 12:44:30',
                'updated_at' => '2017-02-14 12:44:30',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Xander Dach',
                'details' => 'Ratione animi facere maxime libero molestias recusandae.',
                'created_at' => '2017-02-14 12:44:30',
                'updated_at' => '2017-02-14 12:44:30',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Prof. Geovany Torphy IV',
                'details' => 'Quo officiis et eos ea repellendus neque.',
                'created_at' => '2017-02-14 12:44:30',
                'updated_at' => '2017-02-14 12:44:30',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Miss Vallie Dibbert',
                'details' => 'Tempore quisquam eos harum sequi.',
                'created_at' => '2017-02-14 12:44:30',
                'updated_at' => '2017-02-14 12:44:30',
            ),
        ));
        
        
    }
}