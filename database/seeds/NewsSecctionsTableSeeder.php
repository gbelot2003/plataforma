<?php

use Illuminate\Database\Seeder;

class NewsSecctionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('news_secctions')->delete();
        
        \DB::table('news_secctions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Doloribus doloremque cupiditate reprehenderit expedita nesciunt officia.',
                'created_at' => '2017-02-15 01:53:01',
                'updated_at' => '2017-02-15 01:53:01',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Qui non ea corporis et dignissimos.',
                'created_at' => '2017-02-15 01:53:01',
                'updated_at' => '2017-02-15 01:53:01',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Fugit modi quis nesciunt id autem porro.',
                'created_at' => '2017-02-15 01:53:02',
                'updated_at' => '2017-02-15 01:53:02',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Omnis sit qui culpa maxime ea.',
                'created_at' => '2017-02-15 01:53:02',
                'updated_at' => '2017-02-15 01:53:02',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Rerum tempora qui qui dignissimos ut.',
                'created_at' => '2017-02-15 01:53:02',
                'updated_at' => '2017-02-15 01:53:02',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Deserunt et error ipsum repellendus.',
                'created_at' => '2017-02-15 01:53:02',
                'updated_at' => '2017-02-15 01:53:02',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Ipsa aut et nulla.',
                'created_at' => '2017-02-15 01:53:02',
                'updated_at' => '2017-02-15 01:53:02',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Earum id vitae officia omnis.',
                'created_at' => '2017-02-15 01:53:02',
                'updated_at' => '2017-02-15 01:53:02',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Architecto rerum voluptas eos et.',
                'created_at' => '2017-02-15 01:53:02',
                'updated_at' => '2017-02-15 01:53:02',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Impedit officia laboriosam minima assumenda dolores ut pariatur.',
                'created_at' => '2017-02-15 01:53:02',
                'updated_at' => '2017-02-15 01:53:02',
            ),
        ));
        
        
    }
}