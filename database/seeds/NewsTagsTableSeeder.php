<?php

use Illuminate\Database\Seeder;

class NewsTagsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('news_tags')->delete();
        
        \DB::table('news_tags')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Blanditiis et numquam eaque asperiores voluptatem aut itaque.',
                'created_at' => '2017-02-15 01:53:04',
                'updated_at' => '2017-02-15 01:53:04',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Omnis ex doloribus dolor quia sed repellat.',
                'created_at' => '2017-02-15 01:53:04',
                'updated_at' => '2017-02-15 01:53:04',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Voluptatum qui dolorem qui ullam.',
                'created_at' => '2017-02-15 01:53:04',
                'updated_at' => '2017-02-15 01:53:04',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Autem nisi recusandae fugit vitae recusandae in.',
                'created_at' => '2017-02-15 01:53:04',
                'updated_at' => '2017-02-15 01:53:04',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Illo voluptas optio cumque mollitia voluptatem.',
                'created_at' => '2017-02-15 01:53:04',
                'updated_at' => '2017-02-15 01:53:04',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Aut et ratione tempora officiis.',
                'created_at' => '2017-02-15 01:53:04',
                'updated_at' => '2017-02-15 01:53:04',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Rerum ducimus et sit repellat.',
                'created_at' => '2017-02-15 01:53:04',
                'updated_at' => '2017-02-15 01:53:04',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Quidem iusto eaque dolorem iste blanditiis numquam.',
                'created_at' => '2017-02-15 01:53:04',
                'updated_at' => '2017-02-15 01:53:04',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Odio aut tempora magnam non sint.',
                'created_at' => '2017-02-15 01:53:04',
                'updated_at' => '2017-02-15 01:53:04',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Facilis nihil porro omnis molestias ullam soluta.',
                'created_at' => '2017-02-15 01:53:04',
                'updated_at' => '2017-02-15 01:53:04',
            ),
        ));
        
        
    }
}