<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Gerardo Antonio Belot',
                'email' => 'gbelot2003@hotmail.com',
                'password' => '$2y$10$dpmCuxsjcLzaDKJ2QgKOXOU/l/l8gjptTUDqemrqJUk9MT.cW0acq',
                'verified' => 1,
                'token' => 'rC204u3YgBEAzdImsiddpmMWib84S3',
                'status' => 1,
                'remember_token' => 'jAZRXZAiL2OBYkAbeIDwQGz8uBRVq9sZv7h1qsY5Ni7mDad9Rya8XxHbh7O2',
                'created_at' => '2016-12-27 06:08:53',
                'updated_at' => '2017-01-03 21:45:04',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Mrs. Malinda Yundt DDS',
                'email' => 'vita.larson@example.com',
                'password' => '$2y$10$hUF./RO9Riib/pRwrMUzbOb/ZQXEd5jLvJIoU4KRsj7qduirV1R8.',
                'verified' => 1,
                'token' => '5yi37NclJiIk30KcwCqnoS2U4A9GXD',
                'status' => 1,
                'remember_token' => 'Cs5pxd4LPW',
                'created_at' => '2016-12-30 00:19:51',
                'updated_at' => '2016-12-30 00:19:51',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Bailee Rau',
                'email' => 'winnifred.nikolaus@example.org',
                'password' => '$2y$10$hUF./RO9Riib/pRwrMUzbOb/ZQXEd5jLvJIoU4KRsj7qduirV1R8.',
                'verified' => 1,
                'token' => 'lD3GVazFvW0J5SPHdDUpfx05dFHXw1',
                'status' => 1,
                'remember_token' => 'q47B3P1k7k',
                'created_at' => '2016-12-30 00:19:51',
                'updated_at' => '2016-12-30 00:19:51',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Lauriane Hagenes',
                'email' => 'berge.zachariah@example.com',
                'password' => '$2y$10$hUF./RO9Riib/pRwrMUzbOb/ZQXEd5jLvJIoU4KRsj7qduirV1R8.',
                'verified' => 1,
                'token' => 'knG3TdfiQe3yTNnk5fSQsXbCz68CIE',
                'status' => 1,
                'remember_token' => 'qEWh0EzSUA',
                'created_at' => '2016-12-30 00:19:51',
                'updated_at' => '2016-12-30 00:19:51',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Mr. Olen Ortiz V',
                'email' => 'lueilwitz.bessie@example.org',
                'password' => '$2y$10$hUF./RO9Riib/pRwrMUzbOb/ZQXEd5jLvJIoU4KRsj7qduirV1R8.',
                'verified' => 1,
                'token' => '1ODKlsMOayBKCYmlMdNHM6glNG2usf',
                'status' => 1,
                'remember_token' => 'qv8Yf2u0zZ',
                'created_at' => '2016-12-30 00:19:51',
                'updated_at' => '2016-12-30 00:19:51',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Xavier Hansen',
                'email' => 'vesta.auer@example.com',
                'password' => '$2y$10$hUF./RO9Riib/pRwrMUzbOb/ZQXEd5jLvJIoU4KRsj7qduirV1R8.',
                'verified' => 1,
                'token' => '0uI1YnGJEoOlIa70vqfYLS7UJV8yW3',
                'status' => 1,
                'remember_token' => 'ezocYnBkyr',
                'created_at' => '2016-12-30 00:19:51',
                'updated_at' => '2016-12-30 00:19:51',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Ms. Meda Funk MD',
                'email' => 'leopoldo.donnelly@example.com',
                'password' => '$2y$10$hUF./RO9Riib/pRwrMUzbOb/ZQXEd5jLvJIoU4KRsj7qduirV1R8.',
                'verified' => 1,
                'token' => 'Bmk0VTifacF36o058DKqiuH9lWmfYh',
                'status' => 1,
                'remember_token' => '2W0gGqhcjK',
                'created_at' => '2016-12-30 00:19:51',
                'updated_at' => '2016-12-30 00:19:51',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Prof. Tavares Weimann',
                'email' => 'kylee.schultz@example.com',
                'password' => '$2y$10$hUF./RO9Riib/pRwrMUzbOb/ZQXEd5jLvJIoU4KRsj7qduirV1R8.',
                'verified' => 1,
                'token' => '50x705Z3K0E5RrBUryVonWEm7SKFN2',
                'status' => 1,
                'remember_token' => 'GOi3qtYaxe',
                'created_at' => '2016-12-30 00:19:51',
                'updated_at' => '2016-12-30 00:19:51',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Norval Heller',
                'email' => 'wstark@example.org',
                'password' => '$2y$10$hUF./RO9Riib/pRwrMUzbOb/ZQXEd5jLvJIoU4KRsj7qduirV1R8.',
                'verified' => 1,
                'token' => 'VtaMd2lUPNc34Rz6gnSwpOs6mqCliB',
                'status' => 1,
                'remember_token' => 'PuLMnO4yfd',
                'created_at' => '2016-12-30 00:19:51',
                'updated_at' => '2016-12-30 00:19:51',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Prof. Toy Sanford',
                'email' => 'buckridge.ramona@example.org',
                'password' => '$2y$10$hUF./RO9Riib/pRwrMUzbOb/ZQXEd5jLvJIoU4KRsj7qduirV1R8.',
                'verified' => 1,
                'token' => 'p6CaBI7KsRc2ayHf7GU8pPtHaWCg6B',
                'status' => 1,
                'remember_token' => '4sALb1Lplk',
                'created_at' => '2016-12-30 00:19:51',
                'updated_at' => '2016-12-30 00:19:51',
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'Ms. Marge Sawayn',
                'email' => 'qlakin@example.com',
                'password' => '$2y$10$hUF./RO9Riib/pRwrMUzbOb/ZQXEd5jLvJIoU4KRsj7qduirV1R8.',
                'verified' => 1,
                'token' => '08ywg9fR86hZXN7GcQihjG8gpUzVsM',
                'status' => 1,
                'remember_token' => '0FqBomYrqq',
                'created_at' => '2016-12-30 00:19:51',
                'updated_at' => '2016-12-30 00:19:51',
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'Modesta O\'Hara',
                'email' => 'nettie.hackett@example.net',
                'password' => '$2y$10$hUF./RO9Riib/pRwrMUzbOb/ZQXEd5jLvJIoU4KRsj7qduirV1R8.',
                'verified' => 1,
                'token' => 'fa3qDNBO5KxC8yPrAZAkJNJdUCJbFa',
                'status' => 1,
                'remember_token' => '404omvtb2D',
                'created_at' => '2016-12-30 00:19:51',
                'updated_at' => '2016-12-30 00:19:51',
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'Harvey Huel',
                'email' => 'johnson.morris@example.net',
                'password' => '$2y$10$hUF./RO9Riib/pRwrMUzbOb/ZQXEd5jLvJIoU4KRsj7qduirV1R8.',
                'verified' => 1,
                'token' => 'FsHw86qov6RyamjDxCMRIilVOiIy51',
                'status' => 1,
                'remember_token' => 'kaimabLcTr',
                'created_at' => '2016-12-30 00:19:51',
                'updated_at' => '2016-12-30 00:19:51',
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'Patricia Greenholt',
                'email' => 'dkessler@example.com',
                'password' => '$2y$10$hUF./RO9Riib/pRwrMUzbOb/ZQXEd5jLvJIoU4KRsj7qduirV1R8.',
                'verified' => 1,
                'token' => '2GOy1od6DPRlLQW4k2NadMif3QS1Tv',
                'status' => 1,
                'remember_token' => 'XpNDuubHf8',
                'created_at' => '2016-12-30 00:19:51',
                'updated_at' => '2016-12-30 00:19:51',
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'Gilda Klocko',
                'email' => 'ava42@example.com',
                'password' => '$2y$10$hUF./RO9Riib/pRwrMUzbOb/ZQXEd5jLvJIoU4KRsj7qduirV1R8.',
                'verified' => 1,
                'token' => 'kwQbNV9mMeWYjlbvBUS5ekwVW07PXT',
                'status' => 1,
                'remember_token' => 'UbzZHnrhYT',
                'created_at' => '2016-12-30 00:19:51',
                'updated_at' => '2016-12-30 00:19:51',
            ),
            15 => 
            array (
                'id' => 16,
                'name' => 'Prof. Citlalli O\'Conner',
                'email' => 'brenna71@example.net',
                'password' => '$2y$10$hUF./RO9Riib/pRwrMUzbOb/ZQXEd5jLvJIoU4KRsj7qduirV1R8.',
                'verified' => 1,
                'token' => '1HZfRuCCWt0Q244eRpetKhi5oWu9Tt',
                'status' => 1,
                'remember_token' => 'KS7ahvzjqZ',
                'created_at' => '2016-12-30 00:19:51',
                'updated_at' => '2016-12-30 00:19:51',
            ),
            16 => 
            array (
                'id' => 17,
                'name' => 'Mrs. Amelie Dickens MD',
                'email' => 'laurence.davis@example.com',
                'password' => '$2y$10$hUF./RO9Riib/pRwrMUzbOb/ZQXEd5jLvJIoU4KRsj7qduirV1R8.',
                'verified' => 1,
                'token' => 'd27enYc98yT06lIogfL7Kzw6frDKdS',
                'status' => 1,
                'remember_token' => 'LX2Jm1WcHA',
                'created_at' => '2016-12-30 00:19:51',
                'updated_at' => '2016-12-30 00:19:51',
            ),
            17 => 
            array (
                'id' => 18,
                'name' => 'Mercedes Heidenreich',
                'email' => 'muhammad75@example.net',
                'password' => '$2y$10$hUF./RO9Riib/pRwrMUzbOb/ZQXEd5jLvJIoU4KRsj7qduirV1R8.',
                'verified' => 1,
                'token' => 'V6uxxGn55qqpejDXK81k8pkbOuSMPS',
                'status' => 1,
                'remember_token' => 'm65IIVkbDQ',
                'created_at' => '2016-12-30 00:19:51',
                'updated_at' => '2016-12-30 00:19:51',
            ),
            18 => 
            array (
                'id' => 19,
                'name' => 'Reginald Gaylord',
                'email' => 'rohan.grace@example.org',
                'password' => '$2y$10$hUF./RO9Riib/pRwrMUzbOb/ZQXEd5jLvJIoU4KRsj7qduirV1R8.',
                'verified' => 1,
                'token' => 'eLDooKhUt5DBvPkkfQV6S8Kebg2G94',
                'status' => 1,
                'remember_token' => 'qA7auTeIbT',
                'created_at' => '2016-12-30 00:19:51',
                'updated_at' => '2016-12-30 00:19:51',
            ),
            19 => 
            array (
                'id' => 20,
                'name' => 'Dr. Idell Dare II',
                'email' => 'mccullough.tyrique@example.com',
                'password' => '$2y$10$hUF./RO9Riib/pRwrMUzbOb/ZQXEd5jLvJIoU4KRsj7qduirV1R8.',
                'verified' => 1,
                'token' => 'TJVsP3l17JENknGYKDVCJdfqhdHEkv',
                'status' => 1,
                'remember_token' => 'u4rKic6MXx',
                'created_at' => '2016-12-30 00:19:51',
                'updated_at' => '2016-12-30 00:19:51',
            ),
            20 => 
            array (
                'id' => 21,
                'name' => 'Mr. Freddy Cole',
                'email' => 'estevan21@example.net',
                'password' => '$2y$10$hUF./RO9Riib/pRwrMUzbOb/ZQXEd5jLvJIoU4KRsj7qduirV1R8.',
                'verified' => 1,
                'token' => 'lAshSdmotKrrzDE2JtdNCOSgixhpov',
                'status' => 1,
                'remember_token' => 'aSxLfbN1lA',
                'created_at' => '2016-12-30 00:19:51',
                'updated_at' => '2016-12-30 00:19:51',
            ),
        ));
        
        
    }
}